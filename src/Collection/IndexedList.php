<?php declare(strict_types=1);

namespace Novuso\System\Collection;

use ArrayAccess;
use Novuso\System\Exception\IndexException;
use Novuso\System\Exception\UnderflowException;

/**
 * IndexedList is the interface for the list type
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
interface IndexedList extends ArrayAccess, ItemCollection
{
    /**
     * Adds an item to the end
     *
     * @param mixed $item The item
     *
     * @return void
     */
    public function add($item);

    /**
     * Replaces an item at an index
     *
     * @param int   $index The index
     * @param mixed $item  The item
     *
     * @return void
     *
     * @throws IndexException When the index is not defined
     */
    public function set(int $index, $item);

    /**
     * Retrieves an item by index
     *
     * @param int $index The index
     *
     * @return mixed
     *
     * @throws IndexException When the index is not defined
     */
    public function get(int $index);

    /**
     * Checks if an index is defined
     *
     * @param int $index The index
     *
     * @return bool
     */
    public function has(int $index): bool;

    /**
     * Removes an item by index
     *
     * @param int $index The index
     *
     * @return void
     */
    public function remove(int $index);

    /**
     * Replaces an item at an index
     *
     * @param int   $index The index
     * @param mixed $item  The item
     *
     * @return void
     *
     * @throws IndexException When the index is not defined
     */
    public function offsetSet($index, $item);

    /**
     * Retrieves an item by index
     *
     * @param int $index The index
     *
     * @return mixed
     *
     * @throws IndexException When the index is not defined
     */
    public function offsetGet($index);

    /**
     * Checks if an index is defined
     *
     * @param int $index The index
     *
     * @return bool
     */
    public function offsetExists($index): bool;

    /**
     * Removes an item by index
     *
     * @param int $index The index
     *
     * @return void
     */
    public function offsetUnset($index);

    /**
     * Retrieves the first value
     *
     * @return mixed
     *
     * @throws UnderflowException When the list is empty
     */
    public function first();

    /**
     * Retrieves the last value
     *
     * @return mixed
     *
     * @throws UnderflowException When the list is empty
     */
    public function last();

    /**
     * Sets the internal pointer to the first item
     *
     * @return void
     */
    public function rewind();

    /**
     * Sets the internal pointer to the last item
     *
     * @return void
     */
    public function end();

    /**
     * Checks if the internal pointer is at a valid index
     *
     * @return bool
     */
    public function valid(): bool;

    /**
     * Moves the internal pointer to the next value
     *
     * @return void
     */
    public function next();

    /**
     * Moves the internal pointer to the previous value
     *
     * @return void
     */
    public function prev();

    /**
     * Retrieves the index of the internal pointer
     *
     * Returns null if the internal pointer points beyond the end of the list
     * or the list is empty.
     *
     * @return int|null
     */
    public function key();

    /**
     * Retrieves the value at the internal pointer
     *
     * Returns null if the internal pointer points beyond the end of the list
     * or the list is empty.
     *
     * @return mixed|null
     */
    public function current();
}
