<?php declare(strict_types=1);

namespace Novuso\System\Collection;

use Novuso\System\Collection\Traits\ItemTypeMethods;
use Novuso\System\Collection\Tree\BinarySearchTree;
use Novuso\System\Collection\Tree\RedBlackSearchTree;
use Novuso\System\Exception\DomainException;
use Novuso\System\Type\Comparable;
use Novuso\System\Utility\Test;
use Traversable;

/**
 * SortedSet is an implementation of the ordered set type
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
class SortedSet implements OrderedSet
{
    use ItemTypeMethods;

    /**
     * Binary search tree
     *
     * @var BinarySearchTree
     */
    protected $tree;

    /**
     * Constructs SortedTable
     *
     * If types are not provided, the types are dynamic.
     *
     * The item type must be a fully-qualified class name that implements:
     * Novuso\System\Type\Comparable
     *
     * @param string|null           $itemType The item type
     * @param BinarySearchTree|null $tree     The tree or null for default
     *
     * @throws DomainException When the item type is not a comparable type
     */
    public function __construct(string $itemType = null, BinarySearchTree $tree = null)
    {
        if ($itemType !== null && !Test::implements($itemType, Comparable::class)) {
            $message = sprintf('%s expects $itemType to implement %s', __METHOD__, Comparable::class);
            throw DomainException::create($message);
        }

        $this->setItemType($itemType);
        $this->tree = $tree ?: new RedBlackSearchTree();
    }

    /**
     * {@inheritdoc}
     */
    public static function of(string $itemType = null): ItemCollection
    {
        return new static($itemType);
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return $this->tree->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function count(): int
    {
        return $this->tree->count();
    }

    /**
     * {@inheritdoc}
     */
    public function add(Comparable $item)
    {
        assert(Test::type($item, $this->itemType()), $this->itemTypeError('add', $item));

        $this->tree->set($item, true);
    }

    /**
     * {@inheritdoc}
     */
    public function contains(Comparable $item): bool
    {
        return $this->tree->has($item);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Comparable $item)
    {
        $this->tree->remove($item);
    }

    /**
     * {@inheritdoc}
     */
    public function difference(OrderedSet $other): OrderedSet
    {
        $difference = new static($this->itemType());

        if ($this === $other) {
            return $difference;
        }

        $this->reject([$other, 'contains'])->each([$difference, 'add']);
        $other->reject([$this, 'contains'])->each([$difference, 'add']);

        return $difference;
    }

    /**
     * {@inheritdoc}
     */
    public function intersection(OrderedSet $other): OrderedSet
    {
        $intersection = new static($this->itemType());

        $this->filter([$other, 'contains'])->each([$intersection, 'add']);

        return $intersection;
    }

    /**
     * {@inheritdoc}
     */
    public function complement(OrderedSet $other): OrderedSet
    {
        $complement = new static($this->itemType());

        if ($this === $other) {
            return $complement;
        }

        $other->reject([$this, 'contains'])->each([$complement, 'add']);

        return $complement;
    }

    /**
     * {@inheritdoc}
     */
    public function union(OrderedSet $other): OrderedSet
    {
        $union = new static($this->itemType());

        $this->each([$union, 'add']);
        $other->each([$union, 'add']);

        return $union;
    }

    /**
     * {@inheritdoc}
     */
    public function range(Comparable $lo, Comparable $hi): Traversable
    {
        return $this->tree->rangeKeys($lo, $hi);
    }

    /**
     * {@inheritdoc}
     */
    public function rangeCount(Comparable $lo, Comparable $hi): int
    {
        return $this->tree->rangeCount($lo, $hi);
    }

    /**
     * {@inheritdoc}
     */
    public function min(): Comparable
    {
        return $this->tree->min();
    }

    /**
     * {@inheritdoc}
     */
    public function max(): Comparable
    {
        return $this->tree->max();
    }

    /**
     * {@inheritdoc}
     */
    public function removeMin()
    {
        $this->tree->removeMin();
    }

    /**
     * {@inheritdoc}
     */
    public function removeMax()
    {
        $this->tree->removeMax();
    }

    /**
     * {@inheritdoc}
     */
    public function floor(Comparable $item)
    {
        return $this->tree->floor($item);
    }

    /**
     * {@inheritdoc}
     */
    public function ceiling(Comparable $item)
    {
        return $this->tree->ceiling($item);
    }

    /**
     * {@inheritdoc}
     */
    public function index(Comparable $item): int
    {
        return $this->tree->index($item);
    }

    /**
     * {@inheritdoc}
     */
    public function select(int $index): Comparable
    {
        return $this->tree->select($index);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(): Traversable
    {
        return $this->tree->keys();
    }

    /**
     * {@inheritdoc}
     */
    public function each(callable $callback)
    {
        foreach ($this->getIterator() as $item) {
            call_user_func($callback, $item);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function map(callable $callback, string $itemType = null): ItemCollection
    {
        $set = static::of($itemType);

        foreach ($this->getIterator() as $item) {
            $set->add(call_user_func($callback, $item));
        }

        return $set;
    }

    /**
     * {@inheritdoc}
     */
    public function find(callable $predicate)
    {
        foreach ($this->getIterator() as $item) {
            if (call_user_func($predicate, $item)) {
                return $item;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function filter(callable $predicate): ItemCollection
    {
        $set = static::of($this->itemType());

        foreach ($this->getIterator() as $item) {
            if (call_user_func($predicate, $item)) {
                $set->add($item);
            }
        }

        return $set;
    }

    /**
     * {@inheritdoc}
     */
    public function reject(callable $predicate): ItemCollection
    {
        $set = static::of($this->itemType());

        foreach ($this->getIterator() as $item) {
            if (!call_user_func($predicate, $item)) {
                $set->add($item);
            }
        }

        return $set;
    }

    /**
     * {@inheritdoc}
     */
    public function any(callable $predicate): bool
    {
        foreach ($this->getIterator() as $item) {
            if (call_user_func($predicate, $item)) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function every(callable $predicate): bool
    {
        foreach ($this->getIterator() as $item) {
            if (!call_user_func($predicate, $item)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function partition(callable $predicate): array
    {
        $set1 = static::of($this->itemType());
        $set2 = static::of($this->itemType());

        foreach ($this->getIterator() as $item) {
            if (call_user_func($predicate, $item)) {
                $set1->add($item);
            } else {
                $set2->add($item);
            }
        }

        return [$set1, $set2];
    }
}
