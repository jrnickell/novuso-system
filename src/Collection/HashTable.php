<?php declare(strict_types=1);

namespace Novuso\System\Collection;

use Iterator;
use Novuso\System\Collection\Chain\TableBucketChain;
use Novuso\System\Collection\Traits\KeyValueTypeMethods;
use Novuso\System\Exception\KeyException;
use Novuso\System\Utility\Hasher;
use Novuso\System\Utility\Test;
use Novuso\System\Utility\VarPrinter;
use Traversable;

/**
 * HashTable is an implementation of the symbol table type
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
class HashTable implements SymbolTable
{
    use KeyValueTypeMethods;

    /**
     * Bucket chains
     *
     * @var array
     */
    protected $buckets;

    /**
     * Bucket count
     *
     * @var int
     */
    protected $count;

    /**
     * Constructs HashTable
     *
     * If types are not provided, the types are dynamic.
     *
     * The type can be any fully-qualified class or interface name,
     * or one of the following type strings:
     * [array, object, bool, int, float, string, callable]
     *
     * @param string|null $keyType   The key type
     * @param string|null $valueType The value type
     */
    public function __construct(string $keyType = null, string $valueType = null)
    {
        $this->setKeyType($keyType);
        $this->setValueType($valueType);
        $this->buckets = [];
        $this->count = 0;
    }

    /**
     * {@inheritdoc}
     */
    public static function of(string $keyType = null, string $valueType = null): KeyValueCollection
    {
        return new static($keyType, $valueType);
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return $this->count === 0;
    }

    /**
     * {@inheritdoc}
     */
    public function count(): int
    {
        return $this->count;
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value)
    {
        assert(Test::type($key, $this->keyType()), $this->keyTypeError('set', $key));
        assert(Test::type($value, $this->valueType()), $this->valueTypeError('set', $value));

        $hash = Hasher::hash($key);

        if (!isset($this->buckets[$hash])) {
            $this->buckets[$hash] = new TableBucketChain();
        }

        if ($this->buckets[$hash]->set($key, $value)) {
            $this->count++;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        $hash = Hasher::hash($key);

        if (!isset($this->buckets[$hash])) {
            $message = sprintf('Key not found: %s', VarPrinter::toString($key));
            throw KeyException::create($message);
        }

        return $this->buckets[$hash]->get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function has($key): bool
    {
        $hash = Hasher::hash($key);

        if (!isset($this->buckets[$hash])) {
            return false;
        }

        return $this->buckets[$hash]->has($key);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key)
    {
        $hash = Hasher::hash($key);

        if (isset($this->buckets[$hash])) {
            if ($this->buckets[$hash]->remove($key)) {
                $this->count--;
                if ($this->buckets[$hash]->isEmpty()) {
                    unset($this->buckets[$hash]);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($key, $value)
    {
        $this->set($key, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($key)
    {
        return $this->get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($key): bool
    {
        return $this->has($key);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($key)
    {
        $this->remove($key);
    }

    /**
     * {@inheritdoc}
     */
    public function keys(): Traversable
    {
        $iterator = function ($buckets) {
            foreach ($buckets as $chain) {
                for ($chain->rewind(); $chain->valid(); $chain->next()) {
                    yield $chain->key();
                }
            }
        };

        return $iterator($this->buckets);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(): Traversable
    {
        return new class($this) implements Iterator
        {
            /**
             * Symbol table
             *
             * @var SymbolTable
             */
            protected $table;

            /**
             * Table keys
             *
             * @var Iterator
             */
            protected $keys;

            /**
             * Constructor
             *
             * @param SymbolTable $table The symbol table
             */
            public function __construct(SymbolTable $table)
            {
                $this->table = $table;
                $keys = $this->table->keys();
                $this->keys = $this->getKeysIterator($keys);
                $this->keys->rewind();
            }

            /**
             * Rewinds to the first element
             *
             * @return void
             */
            public function rewind()
            {
                $this->keys->rewind();
            }

            /**
             * Checks if current position is valid
             *
             * @return bool
             */
            public function valid(): bool
            {
                return $this->keys->valid();
            }

            /**
             * Moves to the next element
             *
             * @return void
             */
            public function next()
            {
                $this->keys->next();
            }

            /**
             * Retrieves the current key
             *
             * Returns null if the current position is not valid.
             *
             * @return mixed|null
             */
            public function key()
            {
                if (!$this->keys->valid()) {
                    return null;
                }

                return $this->keys->current();
            }

            /**
             * Retrieves the current element
             *
             * Returns null if the current position is not valid.
             *
             * @return mixed|null
             */
            public function current()
            {
                if (!$this->keys->valid()) {
                    return null;
                }

                return $this->table->get($this->keys->current());
            }

            /**
             * Retrieves the iterator for keys
             *
             * @codeCoverageIgnore
             *
             * @param Traversable $keys The traversable keys
             *
             * @return Iterator
             */
            protected function getKeysIterator(Traversable $keys): Iterator
            {
                if ($keys instanceof Iterator) {
                    return $keys;
                }

                return $this->getKeysIterator($keys->getIterator());
            }
        };
    }

    /**
     * {@inheritdoc}
     */
    public function each(callable $callback)
    {
        foreach ($this->getIterator() as $key => $value) {
            call_user_func($callback, $value, $key);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function map(callable $callback, string $valueType = null): KeyValueCollection
    {
        $table = static::of($this->keyType(), $valueType);

        foreach ($this->getIterator() as $key => $value) {
            $table->set($key, call_user_func($callback, $value, $key));
        }

        return $table;
    }

    /**
     * {@inheritdoc}
     */
    public function find(callable $predicate)
    {
        foreach ($this->getIterator() as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                return $key;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function filter(callable $predicate): KeyValueCollection
    {
        $table = static::of($this->keyType(), $this->valueType());

        foreach ($this->getIterator() as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                $table->set($key, $value);
            }
        }

        return $table;
    }

    /**
     * {@inheritdoc}
     */
    public function reject(callable $predicate): KeyValueCollection
    {
        $table = static::of($this->keyType(), $this->valueType());

        foreach ($this->getIterator() as $key => $value) {
            if (!call_user_func($predicate, $value, $key)) {
                $table->set($key, $value);
            }
        }

        return $table;
    }

    /**
     * {@inheritdoc}
     */
    public function any(callable $predicate): bool
    {
        foreach ($this->getIterator() as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function every(callable $predicate): bool
    {
        foreach ($this->getIterator() as $key => $value) {
            if (!call_user_func($predicate, $value, $key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function partition(callable $predicate): array
    {
        $table1 = static::of($this->keyType(), $this->valueType());
        $table2 = static::of($this->keyType(), $this->valueType());

        foreach ($this->getIterator() as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                $table1->set($key, $value);
            } else {
                $table2->set($key, $value);
            }
        }

        return [$table1, $table2];
    }
}
