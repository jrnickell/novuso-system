<?php declare(strict_types=1);

namespace Novuso\System\Collection;

use Iterator;
use Novuso\System\Collection\Traits\KeyValueTypeMethods;
use Novuso\System\Collection\Tree\BinarySearchTree;
use Novuso\System\Collection\Tree\RedBlackSearchTree;
use Novuso\System\Type\Comparable;
use Novuso\System\Utility\Test;
use Novuso\System\Utility\VarPrinter;
use Traversable;

/**
 * SortedTable is an implementation of the ordered table type
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
class SortedTable implements OrderedTable
{
    use KeyValueTypeMethods;

    /**
     * Binary search tree
     *
     * @var BinarySearchTree
     */
    protected $tree;

    /**
     * Constructs SortedTable
     *
     * If types are not provided, the types are dynamic.
     *
     * The key type must be a fully-qualified class name that implements:
     * Novuso\System\Type\Comparable
     *
     * The value type can be any fully-qualified class or interface name,
     * or one of the following type strings:
     * [array, object, bool, int, float, string, callable]
     *
     * @param string|null           $keyType   The key type
     * @param string|null           $valueType The value type
     * @param BinarySearchTree|null $tree      The tree or null for default
     */
    public function __construct(string $keyType = null, string $valueType = null, BinarySearchTree $tree = null)
    {
        $this->setKeyType($keyType);
        $this->setValueType($valueType);
        $this->tree = $tree ?: new RedBlackSearchTree();
    }

    /**
     * Creates instance of with specific key and value types
     *
     * If types are not provided, the types are dynamic.
     *
     * The key type must be a fully-qualified class name that implements:
     * Novuso\System\Type\Comparable
     *
     * The value type can be any fully-qualified class or interface name,
     * or one of the following type strings:
     * [array, object, bool, int, float, string, callable]
     *
     * @param string|null $keyType   The key type
     * @param string|null $valueType The value type
     *
     * @return KeyValueCollection
     */
    public static function of(string $keyType = null, string $valueType = null): KeyValueCollection
    {
        return new static($keyType, $valueType);
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return $this->tree->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function count(): int
    {
        return $this->tree->count();
    }

    /**
     * {@inheritdoc}
     */
    public function set(Comparable $key, $value)
    {
        assert(Test::type($key, $this->keyType()), $this->keyTypeError('set', $key));
        assert(Test::type($value, $this->valueType()), $this->valueTypeError('set', $value));

        $this->tree->set($key, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function get(Comparable $key)
    {
        return $this->tree->get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function has(Comparable $key): bool
    {
        return $this->tree->has($key);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Comparable $key)
    {
        $this->tree->remove($key);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($key, $value)
    {
        assert(Test::comparable($key), sprintf('Illegal offset: %s', VarPrinter::toString($key)));

        $this->set($key, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($key)
    {
        assert(Test::comparable($key), sprintf('Illegal offset: %s', VarPrinter::toString($key)));

        return $this->get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($key): bool
    {
        assert(Test::comparable($key), sprintf('Illegal offset: %s', VarPrinter::toString($key)));

        return $this->has($key);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($key)
    {
        assert(Test::comparable($key), sprintf('Illegal offset: %s', VarPrinter::toString($key)));

        $this->remove($key);
    }

    /**
     * {@inheritdoc}
     */
    public function keys(): Traversable
    {
        return $this->tree->keys();
    }

    /**
     * {@inheritdoc}
     */
    public function rangeKeys(Comparable $lo, Comparable $hi): Traversable
    {
        return $this->tree->rangeKeys($lo, $hi);
    }

    /**
     * {@inheritdoc}
     */
    public function rangeCount(Comparable $lo, Comparable $hi): int
    {
        return $this->tree->rangeCount($lo, $hi);
    }

    /**
     * {@inheritdoc}
     */
    public function min(): Comparable
    {
        return $this->tree->min();
    }

    /**
     * {@inheritdoc}
     */
    public function max(): Comparable
    {
        return $this->tree->max();
    }

    /**
     * {@inheritdoc}
     */
    public function removeMin()
    {
        $this->tree->removeMin();
    }

    /**
     * {@inheritdoc}
     */
    public function removeMax()
    {
        $this->tree->removeMax();
    }

    /**
     * {@inheritdoc}
     */
    public function floor(Comparable $key)
    {
        return $this->tree->floor($key);
    }

    /**
     * {@inheritdoc}
     */
    public function ceiling(Comparable $key)
    {
        return $this->tree->ceiling($key);
    }

    /**
     * {@inheritdoc}
     */
    public function index(Comparable $key): int
    {
        return $this->tree->index($key);
    }

    /**
     * {@inheritdoc}
     */
    public function select(int $index): Comparable
    {
        return $this->tree->select($index);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(): Traversable
    {
        return new class($this) implements Iterator
        {
            /**
             * Ordered table
             *
             * @var OrderedTable
             */
            protected $table;

            /**
             * Table keys
             *
             * @var Iterator
             */
            protected $keys;

            /**
             * Constructor
             *
             * @param OrderedTable $table The ordered table
             */
            public function __construct(OrderedTable $table)
            {
                $this->table = $table;
                $keys = $this->table->keys();
                $this->keys = $this->getKeysIterator($keys);
                $this->keys->rewind();
            }

            /**
             * Rewinds to the first element
             *
             * @return void
             */
            public function rewind()
            {
                $this->keys->rewind();
            }

            /**
             * Checks if current position is valid
             *
             * @return bool
             */
            public function valid(): bool
            {
                return $this->keys->valid();
            }

            /**
             * Moves to the next element
             *
             * @return void
             */
            public function next()
            {
                $this->keys->next();
            }

            /**
             * Retrieves the current key
             *
             * Returns null if the current position is not valid.
             *
             * @return Comparable|null
             */
            public function key()
            {
                if (!$this->keys->valid()) {
                    return null;
                }

                return $this->keys->current();
            }

            /**
             * Retrieves the current element
             *
             * Returns null if the current position is not valid.
             *
             * @return mixed|null
             */
            public function current()
            {
                if (!$this->keys->valid()) {
                    return null;
                }

                return $this->table->get($this->keys->current());
            }

            /**
             * Retrieves the iterator for keys
             *
             * @codeCoverageIgnore
             *
             * @param Traversable $keys The traversable keys
             *
             * @return Iterator
             */
            protected function getKeysIterator(Traversable $keys): Iterator
            {
                if ($keys instanceof Iterator) {
                    return $keys;
                }

                return $this->getKeysIterator($keys->getIterator());
            }
        };
    }

    /**
     * {@inheritdoc}
     */
    public function each(callable $callback)
    {
        foreach ($this->getIterator() as $key => $value) {
            call_user_func($callback, $value, $key);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function map(callable $callback, string $valueType = null): KeyValueCollection
    {
        $table = static::of($this->keyType(), $valueType);

        foreach ($this->getIterator() as $key => $value) {
            $table->set($key, call_user_func($callback, $value, $key));
        }

        return $table;
    }

    /**
     * {@inheritdoc}
     */
    public function find(callable $predicate)
    {
        foreach ($this->getIterator() as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                return $key;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function filter(callable $predicate): KeyValueCollection
    {
        $table = static::of($this->keyType(), $this->valueType());

        foreach ($this->getIterator() as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                $table->set($key, $value);
            }
        }

        return $table;
    }

    /**
     * {@inheritdoc}
     */
    public function reject(callable $predicate): KeyValueCollection
    {
        $table = static::of($this->keyType(), $this->valueType());

        foreach ($this->getIterator() as $key => $value) {
            if (!call_user_func($predicate, $value, $key)) {
                $table->set($key, $value);
            }
        }

        return $table;
    }

    /**
     * {@inheritdoc}
     */
    public function any(callable $predicate): bool
    {
        foreach ($this->getIterator() as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function every(callable $predicate): bool
    {
        foreach ($this->getIterator() as $key => $value) {
            if (!call_user_func($predicate, $value, $key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function partition(callable $predicate): array
    {
        $table1 = static::of($this->keyType(), $this->valueType());
        $table2 = static::of($this->keyType(), $this->valueType());

        foreach ($this->getIterator() as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                $table1->set($key, $value);
            } else {
                $table2->set($key, $value);
            }
        }

        return [$table1, $table2];
    }
}
