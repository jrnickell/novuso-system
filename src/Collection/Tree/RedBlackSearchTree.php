<?php declare(strict_types=1);

namespace Novuso\System\Collection\Tree;

use Novuso\System\Collection\LinkedQueue;
use Novuso\System\Collection\Queue;
use Novuso\System\Exception\IndexException;
use Novuso\System\Exception\KeyException;
use Novuso\System\Exception\UnderflowException;
use Novuso\System\Type\Comparable;
use Novuso\System\Utility\VarPrinter;
use Traversable;

/**
 * RedBlackSearchTree is an implementation of a binary search tree
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
class RedBlackSearchTree implements BinarySearchTree
{
    /**
     * Root node
     *
     * @var RedBlackNode|null
     */
    protected $root;

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return $this->root === null;
    }

    /**
     * {@inheritdoc}
     */
    public function count(): int
    {
        return $this->nodeSize($this->root);
    }

    /**
     * {@inheritdoc}
     */
    public function set(Comparable $key, $value)
    {
        $this->root = $this->nodeSet($key, $value, $this->root);
        $this->root->setColor(RedBlackNode::BLACK);
    }

    /**
     * {@inheritdoc}
     */
    public function get(Comparable $key)
    {
        $node = $this->nodeGet($key, $this->root);

        if ($node === null) {
            $message = sprintf('Key not found: %s', VarPrinter::toString($key));
            throw KeyException::create($message);
        }

        return $node->value();
    }

    /**
     * {@inheritdoc}
     */
    public function has(Comparable $key): bool
    {
        return $this->nodeGet($key, $this->root) !== null;
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Comparable $key)
    {
        if (!$this->has($key)) {
            return;
        }

        if (!$this->isRed($this->root->left()) && !$this->isRed($this->root->right())) {
            $this->root->setColor(RedBlackNode::RED);
        }

        $this->root = $this->nodeRemove($key, $this->root);

        if (!$this->isEmpty()) {
            $this->root->setColor(RedBlackNode::BLACK);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function keys(): Traversable
    {
        if ($this->isEmpty()) {
            return new LinkedQueue();
        }

        return $this->rangeKeys($this->min(), $this->max());
    }

    /**
     * {@inheritdoc}
     */
    public function rangeKeys(Comparable $lo, Comparable $hi): Traversable
    {
        $queue = new LinkedQueue();

        $this->fillKeys($queue, $lo, $hi, $this->root);

        return $queue;
    }

    /**
     * {@inheritdoc}
     */
    public function rangeCount(Comparable $lo, Comparable $hi): int
    {
        if ($lo->compareTo($hi) > 0) {
            return 0;
        }

        if ($this->has($hi)) {
            return $this->index($hi) - $this->index($lo) + 1;
        }

        return $this->index($hi) - $this->index($lo);
    }

    /**
     * {@inheritdoc}
     */
    public function min(): Comparable
    {
        if ($this->isEmpty()) {
            throw UnderflowException::create('Tree underflow');
        }

        return $this->nodeMin($this->root)->key();
    }

    /**
     * {@inheritdoc}
     */
    public function max(): Comparable
    {
        if ($this->isEmpty()) {
            throw UnderflowException::create('Tree underflow');
        }

        return $this->nodeMax($this->root)->key();
    }

    /**
     * {@inheritdoc}
     */
    public function removeMin()
    {
        if ($this->isEmpty()) {
            throw UnderflowException::create('Tree underflow');
        }

        if (!$this->isRed($this->root->left()) && !$this->isRed($this->root->right())) {
            $this->root->setColor(RedBlackNode::RED);
        }

        $this->root = $this->nodeRemoveMin($this->root);

        if (!$this->isEmpty()) {
            $this->root->setColor(RedBlackNode::BLACK);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeMax()
    {
        if ($this->isEmpty()) {
            throw UnderflowException::create('Tree underflow');
        }

        if (!$this->isRed($this->root->left()) && !$this->isRed($this->root->right())) {
            $this->root->setColor(RedBlackNode::RED);
        }

        $this->root = $this->nodeRemoveMax($this->root);

        if (!$this->isEmpty()) {
            $this->root->setColor(RedBlackNode::BLACK);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function floor(Comparable $key)
    {
        if ($this->isEmpty()) {
            throw UnderflowException::create('Tree underflow');
        }

        $node = $this->nodeFloor($key, $this->root);

        if ($node === null) {
            return null;
        }

        return $node->key();
    }

    /**
     * {@inheritdoc}
     */
    public function ceiling(Comparable $key)
    {
        if ($this->isEmpty()) {
            throw UnderflowException::create('Tree underflow');
        }

        $node = $this->nodeCeiling($key, $this->root);

        if ($node === null) {
            return null;
        }

        return $node->key();
    }

    /**
     * {@inheritdoc}
     */
    public function index(Comparable $key): int
    {
        return $this->nodeIndex($key, $this->root);
    }

    /**
     * {@inheritdoc}
     */
    public function select(int $index): Comparable
    {
        $index = (int) $index;

        if ($index < 0 || $index >= $this->nodeSize($this->root)) {
            $message = sprintf('Invalid index: %d', $index);
            throw IndexException::create($message);
        }

        return $this->nodeSelect($index, $this->root)->key();
    }

    /**
     * Checks if a node is red
     *
     * @param RedBlackNode|null $node The node
     *
     * @return bool
     */
    protected function isRed(RedBlackNode $node = null): bool
    {
        if ($node === null) {
            return false;
        }

        return $node->color() === RedBlackNode::RED;
    }

    /**
     * Retrieves the size of a subtree
     *
     * @param RedBlackNode|null $node The subtree root
     *
     * @return int
     */
    protected function nodeSize(RedBlackNode $node = null): int
    {
        if ($node === null) {
            return 0;
        }

        return $node->size();
    }

    /**
     * Inserts a key-value pair in a subtree
     *
     * @param Comparable        $key   The key
     * @param mixed             $value The value
     * @param RedBlackNode|null $node  The subtree root
     *
     * @return RedBlackNode
     */
    protected function nodeSet(Comparable $key, $value, RedBlackNode $node = null): RedBlackNode
    {
        if ($node === null) {
            return new RedBlackNode($key, $value, 1, RedBlackNode::RED);
        }

        $comp = $key->compareTo($node->key());

        if ($comp < 0) {
            $node->setLeft($this->nodeSet($key, $value, $node->left()));
        } elseif ($comp > 0) {
            $node->setRight($this->nodeSet($key, $value, $node->right()));
        } else {
            $node->setValue($value);
        }

        return $this->balanceOnInsert($node);
    }

    /**
     * Retrieves a node by key and subtree
     *
     * Returns null if the node is not found.
     *
     * @param Comparable        $key  The key
     * @param RedBlackNode|null $node The subtree root
     *
     * @return RedBlackNode|null
     */
    protected function nodeGet(Comparable $key, RedBlackNode $node = null)
    {
        while ($node !== null) {
            $comp = $key->compareTo($node->key());

            if ($comp < 0) {
                $node = $node->left();
            } elseif ($comp > 0) {
                $node = $node->right();
            } else {
                return $node;
            }
        }

        return null;
    }

    /**
     * Deletes a node by key and subtree
     *
     * @param Comparable   $key  The key
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode|null
     */
    protected function nodeRemove(Comparable $key, RedBlackNode $node)
    {
        if ($key->compareTo($node->key()) < 0) {
            $node = $this->nodeRemoveLeft($key, $node);
        } else {
            if ($this->isRed($node->left())) {
                $node = $this->rotateRight($node);
            }
            if ($key->compareTo($node->key()) === 0 && $node->right() === null) {
                return null;
            }
            $node = $this->nodeRemoveRight($key, $node);
        }

        return $this->balanceOnRemove($node);
    }

    /**
     * Deletes a node to the left by key and subtree
     *
     * @internal Helper for nodeRemove()
     *
     * @param Comparable   $key  The key
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode
     */
    protected function nodeRemoveLeft(Comparable $key, RedBlackNode $node): RedBlackNode
    {
        if (!$this->isRed($node->left()) && !$this->isRed($node->left()->left())) {
            $node = $this->moveRedLeft($node);
        }
        $node->setLeft($this->nodeRemove($key, $node->left()));

        return $node;
    }

    /**
     * Deletes a node to the right by key and subtree
     *
     * @internal Helper for nodeRemove()
     *
     * @param Comparable   $key  The key
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode
     */
    protected function nodeRemoveRight(Comparable $key, RedBlackNode $node): RedBlackNode
    {
        if (!$this->isRed($node->right()) && !$this->isRed($node->right()->left())) {
            $node = $this->moveRedRight($node);
        }
        if ($key->compareTo($node->key()) === 0) {
            $link = $this->nodeMin($node->right());
            $node->setKey($link->key());
            $node->setValue($link->value());
            $node->setRight($this->nodeRemoveMin($node->right()));
        } else {
            $node->setRight($this->nodeRemove($key, $node->right()));
        }

        return $node;
    }

    /**
     * Fills a queue with keys between lo and hi in a subtree
     *
     * @param Queue             $queue The queue
     * @param Comparable        $lo    The lower bound
     * @param Comparable        $hi    The upper bound
     * @param RedBlackNode|null $node  The subtree root
     *
     * @return void
     */
    protected function fillKeys(Queue $queue, Comparable $lo, Comparable $hi, RedBlackNode $node = null)
    {
        if ($node === null) {
            return;
        }

        $complo = $lo->compareTo($node->key());
        $comphi = $hi->compareTo($node->key());

        if ($complo < 0) {
            $this->fillKeys($queue, $lo, $hi, $node->left());
        }
        if ($complo <= 0 && $comphi >= 0) {
            $queue->enqueue($node->key());
        }
        if ($comphi > 0) {
            $this->fillKeys($queue, $lo, $hi, $node->right());
        }
    }

    /**
     * Retrieves the node with the minimum key in a subtree
     *
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode
     */
    protected function nodeMin(RedBlackNode $node): RedBlackNode
    {
        if ($node->left() === null) {
            return $node;
        }

        return $this->nodeMin($node->left());
    }

    /**
     * Retrieves the node with the maximum key in a subtree
     *
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode
     */
    protected function nodeMax(RedBlackNode $node): RedBlackNode
    {
        if ($node->right() === null) {
            return $node;
        }

        return $this->nodeMax($node->right());
    }

    /**
     * Removes the node with the minimum key in a subtree
     *
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode|null
     */
    protected function nodeRemoveMin(RedBlackNode $node)
    {
        if ($node->left() === null) {
            return null;
        }

        if (!$this->isRed($node->left()) && !$this->isRed($node->left()->left())) {
            $node = $this->moveRedLeft($node);
        }

        $node->setLeft($this->nodeRemoveMin($node->left()));

        return $this->balanceOnRemove($node);
    }

    /**
     * Removes the node with the maximum key in a subtree
     *
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode|null
     */
    protected function nodeRemoveMax(RedBlackNode $node)
    {
        if ($this->isRed($node->left())) {
            $node = $this->rotateRight($node);
        }

        if ($node->right() === null) {
            return null;
        }

        if (!$this->isRed($node->right()) && !$this->isRed($node->right()->left())) {
            $node = $this->moveRedRight($node);
        }

        $node->setRight($this->nodeRemoveMax($node->right()));

        return $this->balanceOnRemove($node);
    }

    /**
     * Retrieves the node with the largest key <= to a key in a subtree
     *
     * Returns null if there is not a key less or equal to the given key.
     *
     * @param Comparable        $key  The key
     * @param RedBlackNode|null $node The subtree root
     *
     * @return RedBlackNode|null
     */
    protected function nodeFloor(Comparable $key, RedBlackNode $node = null)
    {
        if ($node === null) {
            return null;
        }

        $comp = $key->compareTo($node->key());

        if ($comp === 0) {
            return $node;
        }
        if ($comp < 0) {
            return $this->nodeFloor($key, $node->left());
        }
        $right = $this->nodeFloor($key, $node->right());
        if ($right !== null) {
            return $right;
        }

        return $node;
    }

    /**
     * Retrieves the node with the smallest key >= to a key in a subtree
     *
     * Returns null if there is not a key less or equal to the given key.
     *
     * @param Comparable        $key  The key
     * @param RedBlackNode|null $node The subtree root
     *
     * @return RedBlackNode|null
     */
    protected function nodeCeiling(Comparable $key, RedBlackNode $node = null)
    {
        if ($node === null) {
            return null;
        }

        $comp = $key->compareTo($node->key());

        if ($comp === 0) {
            return $node;
        }
        if ($comp > 0) {
            return $this->nodeCeiling($key, $node->right());
        }
        $left = $this->nodeCeiling($key, $node->left());
        if ($left !== null) {
            return $left;
        }

        return $node;
    }

    /**
     * Retrieves the index for a key in a subtree
     *
     * @param Comparable        $key  The key
     * @param RedBlackNode|null $node The subtree root
     *
     * @return int
     */
    protected function nodeIndex(Comparable $key, RedBlackNode $node = null): int
    {
        if ($node === null) {
            return 0;
        }

        $comp = $key->compareTo($node->key());

        if ($comp < 0) {
            return $this->nodeIndex($key, $node->left());
        }
        if ($comp > 0) {
            return 1 + $this->nodeSize($node->left()) + $this->nodeIndex($key, $node->right());
        }

        return $this->nodeSize($node->left());
    }

    /**
     * Retrieves the node with the key of a given index in a subtree
     *
     * @param int          $index The index
     * @param RedBlackNode $node  The subtree root
     *
     * @return RedBlackNode
     */
    protected function nodeSelect(int $index, RedBlackNode $node): RedBlackNode
    {
        $size = $this->nodeSize($node->left());

        if ($size > $index) {
            return $this->nodeSelect($index, $node->left());
        }
        if ($size < $index) {
            return $this->nodeSelect($index - $size - 1, $node->right());
        }

        return $node;
    }

    /**
     * Rotates a right-learning link to the left
     *
     * Assumes $node->right is red.
     *
     * @param RedBlackNode $node The node
     *
     * @return RedBlackNode
     */
    protected function rotateLeft(RedBlackNode $node): RedBlackNode
    {
        $link = $node->right();
        $node->setRight($link->left());
        $link->setLeft($node);
        $link->setColor($node->color());
        $node->setColor(RedBlackNode::RED);
        $link->setSize($node->size());
        $node->setSize(1 + $this->nodeSize($node->left()) + $this->nodeSize($node->right()));

        return $link;
    }

    /**
     * Rotates a left-leaning link to the right
     *
     * Assumes $node->left is red.
     *
     * @param RedBlackNode $node The node
     *
     * @return RedBlackNode
     */
    protected function rotateRight(RedBlackNode $node): RedBlackNode
    {
        $link = $node->left();
        $node->setLeft($link->right());
        $link->setRight($node);
        $link->setColor($node->color());
        $node->setColor(RedBlackNode::RED);
        $link->setSize($node->size());
        $node->setSize(1 + $this->nodeSize($node->left()) + $this->nodeSize($node->right()));

        return $link;
    }

    /**
     * Flips the colors of a node and its two children
     *
     * Used to maintain symmetric order and perfect black balance when a black
     * node has two red children.
     *
     * @param RedBlackNode $node The node
     *
     * @return void
     */
    protected function flipColors(RedBlackNode $node)
    {
        $node->setColor(!($node->color()));
        $node->left()->setColor(!($node->left()->color()));
        $node->right()->setColor(!($node->right()->color()));
    }

    /**
     * Makes a left link or child red
     *
     * Assumes red $node and $node->left and $node->left->left are black.
     *
     * @codeCoverageIgnore
     *
     * @param RedBlackNode $node The node
     *
     * @return RedBlackNode
     */
    protected function moveRedLeft(RedBlackNode $node): RedBlackNode
    {
        $this->flipColors($node);
        if ($this->isRed($node->right()->left())) {
            $node->setRight($this->rotateRight($node->right()));
            $node = $this->rotateLeft($node);
            $this->flipColors($node);
        }

        return $node;
    }

    /**
     * Makes a right link or child red
     *
     * Assumes red $node and $node->right and $node->right->left are black.
     *
     * @codeCoverageIgnore
     *
     * @param RedBlackNode $node The node
     *
     * @return RedBlackNode
     */
    protected function moveRedRight(RedBlackNode $node): RedBlackNode
    {
        $this->flipColors($node);
        if ($this->isRed($node->left()->left())) {
            $node = $this->rotateRight($node);
            $this->flipColors($node);
        }

        return $node;
    }

    /**
     * Restores red-black tree invariant on remove
     *
     * @codeCoverageIgnore
     *
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode
     */
    protected function balanceOnRemove(RedBlackNode $node): RedBlackNode
    {
        if ($this->isRed($node->right())) {
            $node = $this->rotateLeft($node);
        }
        if ($this->isRed($node->left()) && $this->isRed($node->left()->left())) {
            $node = $this->rotateRight($node);
        }
        if ($this->isRed($node->left()) && $this->isRed($node->right())) {
            $this->flipColors($node);
        }
        $node->setSize(1 + $this->nodeSize($node->left()) + $this->nodeSize($node->right()));

        return $node;
    }

    /**
     * Restores red-black tree invariant on insert
     *
     * @codeCoverageIgnore
     *
     * @param RedBlackNode $node The subtree root
     *
     * @return RedBlackNode
     */
    protected function balanceOnInsert(RedBlackNode $node): RedBlackNode
    {
        if ($this->isRed($node->right()) && !$this->isRed($node->left())) {
            $node = $this->rotateLeft($node);
        }
        if ($this->isRed($node->left()) && $this->isRed($node->left()->left())) {
            $node = $this->rotateRight($node);
        }
        if ($this->isRed($node->left()) && $this->isRed($node->right())) {
            $this->flipColors($node);
        }
        $node->setSize(1 + $this->nodeSize($node->left()) + $this->nodeSize($node->right()));

        return $node;
    }
}
