<?php declare(strict_types=1);

namespace Novuso\System\Collection\Tree;

use Countable;
use Novuso\System\Exception\IndexException;
use Novuso\System\Exception\KeyException;
use Novuso\System\Exception\UnderflowException;
use Novuso\System\Type\Comparable;
use Traversable;

/**
 * BinarySearchTree is the interface for a binary search tree
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
interface BinarySearchTree extends Countable
{
    /**
     * Checks if empty
     *
     * @return bool
     */
    public function isEmpty(): bool;

    /**
     * Retrieves the number of nodes
     *
     * @return int
     */
    public function count(): int;

    /**
     * Sets a key-value pair
     *
     * @param Comparable $key   The key
     * @param mixed      $value The value
     *
     * @return void
     */
    public function set(Comparable $key, $value);

    /**
     * Retrieves a value by key
     *
     * @param Comparable $key The key
     *
     * @return mixed
     *
     * @throws KeyException When the key is not defined
     */
    public function get(Comparable $key);

    /**
     * Checks if a key is defined
     *
     * @param Comparable $key The key
     *
     * @return bool
     */
    public function has(Comparable $key): bool;

    /**
     * Removes a key-value pair by key
     *
     * @param Comparable $key The key
     *
     * @return void
     */
    public function remove(Comparable $key);

    /**
     * Retrieves an iterator for keys
     *
     * @return Traversable
     */
    public function keys(): Traversable;

    /**
     * Retrieves an inclusive list of keys between given keys
     *
     * @param Comparable $lo The lower bound
     * @param Comparable $hi The upper bound
     *
     * @return Traversable
     */
    public function rangeKeys(Comparable $lo, Comparable $hi): Traversable;

    /**
     * Retrieves the inclusive number of keys between given keys
     *
     * @param Comparable $lo The lower bound
     * @param Comparable $hi The upper bound
     *
     * @return int
     */
    public function rangeCount(Comparable $lo, Comparable $hi): int;

    /**
     * Retrieves the minimum key
     *
     * @return Comparable
     *
     * @throws UnderflowException When the table is empty
     */
    public function min(): Comparable;

    /**
     * Retrieves the maximum key
     *
     * @return Comparable
     *
     * @throws UnderflowException When the table is empty
     */
    public function max(): Comparable;

    /**
     * Removes the key-value pair with the minimum key
     *
     * @return void
     *
     * @throws UnderflowException When the table is empty
     */
    public function removeMin();

    /**
     * Removes the key-value pair with the maximum key
     *
     * @return void
     *
     * @throws UnderflowException When the table is empty
     */
    public function removeMax();

    /**
     * Retrieves the largest key less or equal to the given key
     *
     * Returns null if there is not a key less or equal to the given key.
     *
     * @param Comparable $key The key
     *
     * @return Comparable|null
     *
     * @throws UnderflowException When the table is empty
     */
    public function floor(Comparable $key);

    /**
     * Retrieves the smallest key greater or equal to the given key
     *
     * Returns null if there is not a key greater or equal to the given key.
     *
     * @param Comparable $key The key
     *
     * @return Comparable|null
     *
     * @throws UnderflowException When the table is empty
     */
    public function ceiling(Comparable $key);

    /**
     * Retrieves the index of the given key
     *
     * @param Comparable $key The key
     *
     * @return int
     */
    public function index(Comparable $key): int;

    /**
     * Retrieves the key with the given index
     *
     * @param int $index The index
     *
     * @return Comparable
     *
     * @throws IndexException When the index is not valid
     */
    public function select(int $index): Comparable;
}
