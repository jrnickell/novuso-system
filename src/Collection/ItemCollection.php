<?php declare(strict_types=1);

namespace Novuso\System\Collection;

/**
 * ItemCollection is the interface for item collections
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
interface ItemCollection extends Collection
{
    /**
     * Creates instance of a specific type
     *
     * If a type is not provided, the item type is dynamic.
     *
     * The type can be any fully-qualified class or interface name,
     * or one of the following type strings:
     * [array, object, bool, int, float, string, callable]
     *
     * @param string|null $itemType The item type
     *
     * @return ItemCollection
     */
    public static function of(string $itemType = null): ItemCollection;

    /**
     * Retrieves the item type
     *
     * Returns null if the collection type is dynamic.
     *
     * @return string|null
     */
    public function itemType();

    /**
     * Applies a callback function to every item
     *
     * Callback signature: function ($item) {}
     *
     * @param callable $callback The callback
     *
     * @return void
     */
    public function each(callable $callback);

    /**
     * Creates a collection from the results of a function
     *
     * Callback signature: function ($item): $newItem {}
     *
     * @param callable    $callback The callback
     * @param string|null $itemType The item type for the new collection
     *
     * @return ItemCollection
     */
    public function map(callable $callback, string $itemType = null): ItemCollection;

    /**
     * Retrieves the first item that passes a truth test
     *
     * Returns null if no item passes the test.
     *
     * Function signature: function ($item): bool {}
     *
     * @param callable $predicate The predicate function
     *
     * @return mixed|null
     */
    public function find(callable $predicate);

    /**
     * Creates a collection from items that pass a truth test
     *
     * Function signature: function ($item): bool {}
     *
     * @param callable $predicate The predicate function
     *
     * @return ItemCollection
     */
    public function filter(callable $predicate): ItemCollection;

    /**
     * Creates a collection from items that fail a truth test
     *
     * Function signature: function ($item): bool {}
     *
     * @param callable $predicate The predicate function
     *
     * @return ItemCollection
     */
    public function reject(callable $predicate): ItemCollection;

    /**
     * Checks if any items pass a truth test
     *
     * Function signature: function ($item): bool {}
     *
     * @param callable $predicate The predicate function
     *
     * @return bool
     */
    public function any(callable $predicate): bool;

    /**
     * Checks if all items pass a truth test
     *
     * Function signature: function ($item): bool {}
     *
     * @param callable $predicate The predicate function
     *
     * @return bool
     */
    public function every(callable $predicate): bool;

    /**
     * Creates two collections based on a truth test
     *
     * Items that pass the truth test are placed in the first collection.
     *
     * Items that fail the truth test are placed in the second collection.
     *
     * Function signature: function ($item): bool {}
     *
     * @param callable $predicate The predicate function
     *
     * @return ItemCollection[]
     */
    public function partition(callable $predicate): array;
}
