<?php declare(strict_types=1);

namespace Novuso\System\Collection;

use Novuso\System\Exception\UnderflowException;

/**
 * Stack is the interface for the stack type
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
interface Stack extends ItemCollection
{
    /**
     * Adds an item to the top
     *
     * @param mixed $item The item
     *
     * @return void
     */
    public function push($item);

    /**
     * Removes and returns the top item
     *
     * @return mixed
     *
     * @throws UnderflowException When the stack is empty
     */
    public function pop();

    /**
     * Retrieves the top item without removal
     *
     * @return mixed
     *
     * @throws UnderflowException When the stack is empty
     */
    public function top();
}
