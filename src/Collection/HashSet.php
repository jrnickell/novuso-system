<?php declare(strict_types=1);

namespace Novuso\System\Collection;

use Novuso\System\Collection\Chain\SetBucketChain;
use Novuso\System\Collection\Traits\ItemTypeMethods;
use Novuso\System\Utility\Hasher;
use Novuso\System\Utility\Test;
use Traversable;

/**
 * HashSet is an implementation of the set type
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
class HashSet implements Set
{
    use ItemTypeMethods;

    /**
     * Bucket chains
     *
     * @var array
     */
    protected $buckets;

    /**
     * Bucket count
     *
     * @var int
     */
    protected $count;

    /**
     * Constructs HashSet
     *
     * If a type is not provided, the item type is dynamic.
     *
     * The type can be any fully-qualified class or interface name,
     * or one of the following type strings:
     * [array, object, bool, int, float, string, callable]
     *
     * @param string|null $itemType The item type or null for dynamic type
     */
    public function __construct(string $itemType = null)
    {
        $this->setItemType($itemType);
        $this->buckets = [];
        $this->count = 0;
    }

    /**
     * {@inheritdoc}
     */
    public static function of(string $itemType = null): ItemCollection
    {
        return new static($itemType);
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return $this->count === 0;
    }

    /**
     * {@inheritdoc}
     */
    public function count(): int
    {
        return $this->count;
    }

    /**
     * {@inheritdoc}
     */
    public function add($item)
    {
        assert(Test::type($item, $this->itemType()), $this->itemTypeError('add', $item));

        $hash = Hasher::hash($item);

        if (!isset($this->buckets[$hash])) {
            $this->buckets[$hash] = new SetBucketChain();
        }

        if ($this->buckets[$hash]->add($item)) {
            $this->count++;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function contains($item): bool
    {
        $hash = Hasher::hash($item);

        if (!isset($this->buckets[$hash])) {
            return false;
        }

        return $this->buckets[$hash]->contains($item);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($item)
    {
        $hash = Hasher::hash($item);

        if (isset($this->buckets[$hash])) {
            if ($this->buckets[$hash]->remove($item)) {
                $this->count--;
                if ($this->buckets[$hash]->isEmpty()) {
                    unset($this->buckets[$hash]);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function difference(Set $other): Set
    {
        $difference = new static($this->itemType());

        if ($this === $other) {
            return $difference;
        }

        $this->reject([$other, 'contains'])->each([$difference, 'add']);
        $other->reject([$this, 'contains'])->each([$difference, 'add']);

        return $difference;
    }

    /**
     * {@inheritdoc}
     */
    public function intersection(Set $other): Set
    {
        $intersection = new static($this->itemType());

        $this->filter([$other, 'contains'])->each([$intersection, 'add']);

        return $intersection;
    }

    /**
     * {@inheritdoc}
     */
    public function complement(Set $other): Set
    {
        $complement = new static($this->itemType());

        if ($this === $other) {
            return $complement;
        }

        $other->reject([$this, 'contains'])->each([$complement, 'add']);

        return $complement;
    }

    /**
     * {@inheritdoc}
     */
    public function union(Set $other): Set
    {
        $union = new static($this->itemType());

        $this->each([$union, 'add']);
        $other->each([$union, 'add']);

        return $union;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(): Traversable
    {
        $iterator = function ($buckets) {
            foreach ($buckets as $chain) {
                for ($chain->rewind(); $chain->valid(); $chain->next()) {
                    yield $chain->current();
                }
            }
        };

        return $iterator($this->buckets);
    }

    /**
     * {@inheritdoc}
     */
    public function each(callable $callback)
    {
        foreach ($this->getIterator() as $item) {
            call_user_func($callback, $item);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function map(callable $callback, string $itemType = null): ItemCollection
    {
        $set = static::of($itemType);

        foreach ($this->getIterator() as $item) {
            $set->add(call_user_func($callback, $item));
        }

        return $set;
    }

    /**
     * {@inheritdoc}
     */
    public function find(callable $predicate)
    {
        foreach ($this->getIterator() as $item) {
            if (call_user_func($predicate, $item)) {
                return $item;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function filter(callable $predicate): ItemCollection
    {
        $set = static::of($this->itemType());

        foreach ($this->getIterator() as $item) {
            if (call_user_func($predicate, $item)) {
                $set->add($item);
            }
        }

        return $set;
    }

    /**
     * {@inheritdoc}
     */
    public function reject(callable $predicate): ItemCollection
    {
        $set = static::of($this->itemType());

        foreach ($this->getIterator() as $item) {
            if (!call_user_func($predicate, $item)) {
                $set->add($item);
            }
        }

        return $set;
    }

    /**
     * {@inheritdoc}
     */
    public function any(callable $predicate): bool
    {
        foreach ($this->getIterator() as $item) {
            if (call_user_func($predicate, $item)) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function every(callable $predicate): bool
    {
        foreach ($this->getIterator() as $item) {
            if (!call_user_func($predicate, $item)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function partition(callable $predicate): array
    {
        $set1 = static::of($this->itemType());
        $set2 = static::of($this->itemType());

        foreach ($this->getIterator() as $item) {
            if (call_user_func($predicate, $item)) {
                $set1->add($item);
            } else {
                $set2->add($item);
            }
        }

        return [$set1, $set2];
    }
}
