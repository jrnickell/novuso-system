<?php declare(strict_types=1);

namespace Novuso\System\Type;

/**
 * Comparable is the interface for types that provide natural order comparison
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
interface Comparable
{
    /**
     * Compares to another object
     *
     * The passed object must be an instance of the same type.
     *
     * The method should return 0 for values considered equal, return -1 if
     * this instance is less than the passed value, and return 1 if this
     * instance is greater than the passed value.
     *
     * @param object $object The object
     *
     * @return int
     */
    public function compareTo($object): int;
}
