<?php declare(strict_types=1);

namespace Novuso\System\Type;

use JsonSerializable;
use Novuso\System\Exception\DomainException;
use Novuso\System\Utility\ClassName;
use Novuso\System\Utility\Test;
use Novuso\System\Utility\VarPrinter;
use ReflectionClass;
use Serializable;

/**
 * Enum is the base class for enum types
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
abstract class Enum implements Comparable, Equatable, JsonSerializable, Serializable
{
    /**
     * Enum value
     *
     * @var mixed
     */
    protected $value;

    /**
     * Enum constant name
     *
     * @var string
     */
    protected $name;

    /**
     * Enum constant ordinal
     *
     * @var int
     */
    protected $ordinal;

    /**
     * Constants cache
     *
     * @var array
     */
    private static $constants;

    /**
     * Constructs Enum
     *
     * @internal
     *
     * @param mixed $value The enum value
     *
     * @throws DomainException When the value is invalid
     */
    final private function __construct($value)
    {
        $constants = self::getMembers();

        if (!in_array($value, $constants, true)) {
            $var = VarPrinter::toString($value);
            $message = sprintf('%s is not a member value of enum %s', $var, static::class);
            throw DomainException::create($message);
        }

        $this->value = $value;
    }

    /*
     * Some of the following methods are derived from the code of php-enum:
     * https://github.com/marc-mabe/php-enum
     * (2.1.0 - 2015-06-22)
     *
     * Copyright (c) 2015, Marc Bennewitz
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without
     * modification, are permitted provided that the following conditions are
     * met:
     *
     *     * Redistributions of source code must retain the above copyright
     *       notice, this list of conditions and the following disclaimer.
     *
     *     * Redistributions in binary form must reproduce the above copyright
     *       notice, this list of conditions and the following disclaimer in
     *       the documentation and/or other materials provided with the
     *       distribution.
     *
     *     * Neither the name of the organisation nor the names of its
     *       contributors may be used to endorse or promote products derived
     *       from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
     * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
     * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
     * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
     * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
     * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
     * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
     * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
     * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
     * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
     * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
     */

    /**
     * Creates instance from an enum constant name
     *
     * Maps static method `MyClass::CONST_NAME()` given `CONST_NAME` is a class
     * constant of `MyClass`.
     *
     * @param string $name The name of the method
     * @param array  $args A list of arguments
     *
     * @return Enum
     *
     * @throws DomainException When the constant name is not defined
     */
    final public static function __callStatic($name, array $args): Enum
    {
        return self::fromName($name);
    }

    /**
     * Creates instance from an enum constant value
     *
     * @param mixed $value The enum constant value
     *
     * @return Enum
     *
     * @throws DomainException When the value is invalid
     */
    final public static function fromValue($value): Enum
    {
        return new static($value);
    }

    /**
     * Creates instance from an enum constant name
     *
     * @param string $name The enum constant name
     *
     * @return Enum
     *
     * @throws DomainException When the name is invalid
     */
    final public static function fromName(string $name): Enum
    {
        $constName = static::class.'::'.$name;

        if (!defined($constName)) {
            $message = sprintf('%s is not a member constant of enum %s', $name, static::class);
            throw DomainException::create($message);
        }

        return new static(constant($constName));
    }

    /**
     * Creates instance from an enum ordinal position
     *
     * @param int $ordinal The enum ordinal position
     *
     * @return Enum
     *
     * @throws DomainException When the ordinal is invalid
     */
    final public static function fromOrdinal(int $ordinal): Enum
    {
        $constants = self::getMembers();
        $item = array_slice($constants, $ordinal, 1, true);

        if (!$item) {
            $end = count($constants) - 1;
            $message = sprintf('Enum ordinal (%d) out of range [0, %d]', $ordinal, $end);
            throw DomainException::create($message);
        }

        return new static(current($item));
    }

    /**
     * Retrieves enum member names and values
     *
     * @codeCoverageIgnore
     *
     * @return array
     *
     * @throws DomainException When more than one constant has the same value
     */
    final public static function getMembers(): array
    {
        if (self::$constants === null) {
            self::$constants = [];
        }

        if (!isset(self::$constants[static::class])) {
            $reflection = new ReflectionClass(static::class);
            self::guardConstants($reflection);
            self::$constants[static::class] = self::sortConstants($reflection);
        }

        return self::$constants[static::class];
    }

    /**
     * Retrieves the enum constant value
     *
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * Retrieves the enum constant name
     *
     * @return string
     */
    public function name(): string
    {
        if ($this->name === null) {
            $constants = self::getMembers();
            $this->name = array_search($this->value(), $constants, true);
        }

        return $this->name;
    }

    /**
     * Retrieves the enum ordinal position
     *
     * @return int
     */
    public function ordinal(): int
    {
        if ($this->ordinal === null) {
            $ordinal = 0;
            $value = $this->value();
            foreach (self::getMembers() as $constValue) {
                if ($value === $constValue) {
                    break;
                }
                $ordinal++;
            }
            $this->ordinal = $ordinal;
        }

        return $this->ordinal;
    }

    /**
     * Retrieves a string representation
     *
     * @return string
     */
    public function toString(): string
    {
        return sprintf('%s.%s', ClassName::short(static::class), $this->name());
    }

    /**
     * Handles casting to a string
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * Retrieves a value for JSON encoding
     *
     * @return mixed
     */
    public function jsonSerialize()
    {
        return $this->value;
    }

    /**
     * Retrieves a serialized representation
     *
     * @return string
     */
    public function serialize(): string
    {
        return serialize(['value' => $this->value]);
    }

    /**
     * Handles construction from a serialized representation
     *
     * @param string $str The serialized representation
     *
     * @return void
     */
    public function unserialize($str)
    {
        $data = unserialize($str);
        $this->__construct($data['value']);
    }

    /**
     * {@inheritdoc}
     */
    public function compareTo($object): int
    {
        if ($this === $object) {
            return 0;
        }

        assert(Test::sameType($this, $object), sprintf('Comparison requires instance of %s', static::class));

        $thisOrd = $this->ordinal();
        $thatOrd = $object->ordinal();

        if ($thisOrd > $thatOrd) {
            return 1;
        }
        if ($thisOrd < $thatOrd) {
            return -1;
        }

        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public function equals($object): bool
    {
        if ($this === $object) {
            return true;
        }

        if (!Test::sameType($this, $object)) {
            return false;
        }

        return $this->name() === $object->name();
    }

    /**
     * {@inheritdoc}
     */
    public function hashValue(): string
    {
        return $this->name();
    }

    /**
     * Validates enum constants
     *
     * @param ReflectionClass $reflection The reflection instance
     *
     * @return void
     */
    final private static function guardConstants(ReflectionClass $reflection)
    {
        $constants = $reflection->getConstants();
        $duplicates = [];
        foreach ($constants as $value) {
            $names = array_keys($constants, $value, true);
            if (count($names) > 1) {
                $duplicates[VarPrinter::toString($value)] = $names;
            }
        }
        if (!empty($duplicates)) {
            $list = array_map(function ($names) use ($constants) {
                return sprintf('(%s)=%s', implode('|', $names), VarPrinter::toString($constants[$names[0]]));
            }, $duplicates);
            $message = sprintf('Duplicate enum values: %s',implode(', ', $list));
            throw DomainException::create($message);
        }
    }

    /**
     * Sorts member constants
     *
     * @param ReflectionClass $reflection The reflection instance
     *
     * @return array
     */
    final private static function sortConstants(ReflectionClass $reflection): array
    {
        $constants = [];
        while ($reflection && __CLASS__ !== $reflection->getName()) {
            $constants = $reflection->getConstants() + $constants;
            $reflection = $reflection->getParentClass();
        }

        return $constants;
    }
}
