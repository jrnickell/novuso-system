<?php declare(strict_types=1);

namespace Novuso\System\Utility;

use Closure;
use DateTime;

/**
 * VarPrinter is a variable printing utility
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
final class VarPrinter
{
    /**
     * Reads a string representation from a value
     *
     * @param mixed $value The value
     *
     * @return string
     */
    public static function toString($value): string
    {
        if ($value === null) {
            return 'NULL';
        }
        if ($value === true) {
            return 'TRUE';
        }
        if ($value === false) {
            return 'FALSE';
        }
        if (is_object($value)) {
            return self::readObject($value);
        }
        if (is_array($value)) {
            return self::readArray($value);
        }
        if (is_resource($value)) {
            return self::readResource($value);
        }

        return (string) $value;
    }

    /**
     * Reads a string representation from an object
     *
     * @param object $obj The object
     *
     * @return string
     */
    private static function readObject($obj): string
    {
        if ($obj instanceof Closure) {
            return 'Function';
        }
        if ($obj instanceof DateTime) {
            return sprintf('DateTime(%s)', $obj->format('Y-m-d\TH:i:sP'));
        }
        if (method_exists($obj, 'toString')) {
            return (string) $obj->toString();
        }
        if (method_exists($obj, '__toString')) {
            return (string) $obj;
        }

        return sprintf('Object(%s)', get_class($obj));
    }

    /**
     * Reads a string representation from an array
     *
     * @param array $arr The array
     *
     * @return string
     */
    private static function readArray(array $arr): string
    {
        $data = [];

        foreach ($arr as $key => $value) {
            $data[] = sprintf('%s => %s', $key, static::toString($value));
        }

        return sprintf('Array(%s)', implode(', ', $data));
    }

    /**
     * Reads a string representation from a resource
     *
     * @param resource $res The resource
     *
     * @return string
     */
    private static function readResource($res): string
    {
        return sprintf('Resource(%s)', get_resource_type($res));
    }
}
