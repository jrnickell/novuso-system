<?php declare(strict_types=1);

namespace Novuso\System\Utility;

use Novuso\System\Exception\TypeException;

/**
 * ClassName is a class name string utility
 *
 * @see       https://github.com/mathiasverraes/classfunctions
 * @copyright Copyright (c) 2015, Mathias Verraes. <http://verraes.net>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    Mathias Verraes <mathias@verraes.net>
 * @version   0.0.2
 */
final class ClassName
{
    /*
     * Copyright (c) 2015 Mathias Verraes
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy
     * of this software and associated documentation files (the "Software"), to
     * deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
     * sell copies of the Software, and to permit persons to whom the Software is
     * furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in
     * all copies or substantial portions of the Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
     * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
     * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
     * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
     * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
     * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
     * IN THE SOFTWARE.
     */

    /**
     * Retrieves the full class name of an object
     *
     * @param object|string $object An object, fully qualified class name, or
     *                              canonical class name
     *
     * @return string
     */
    public static function full($object): string
    {
        if (is_string($object)) {
            return str_replace('.', '\\', $object);
        }

        if (is_object($object)) {
            return trim(get_class($object), '\\');
        }

        $message = sprintf(
            '%s expects $object to be an object or string; received (%s) %s',
            __METHOD__,
            gettype($object),
            VarPrinter::toString($object)
        );
        throw TypeException::create($message);
    }

    /**
     * Retrieves the canonical class name
     *
     * @param object|string $object An object, fully qualified class name, or
     *                              canonical class name
     *
     * @return string
     */
    public static function canonical($object): string
    {
        return str_replace('\\', '.', self::full($object));
    }

    /**
     * Retrieves the lowercase underscored class name
     *
     * @param object|string $object An object, fully qualified class name, or
     *                              canonical class name
     *
     * @return string
     */
    public static function underscore($object): string
    {
        return strtolower(preg_replace('~(?<=\\w)([A-Z])~', '_$1', self::canonical($object)));
    }

    /**
     * Retrieves the short class name
     *
     * @param object|string $object An object, fully qualified class name, or
     *                              canonical class name
     *
     * @return string
     */
    public static function short($object): string
    {
        $parts = explode('\\', self::full($object));

        return end($parts);
    }
}
