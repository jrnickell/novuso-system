<?php declare(strict_types=1);

namespace Novuso\System\Utility;

/**
 * Hasher is a non-cryptographic hashing utility
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
final class Hasher
{
    /**
     * Creates a string hash for a value
     *
     * @param mixed  $value The value
     * @param string $algo  The hash algorithm
     *
     * @return string
     */
    public static function hash($value, string $algo = 'fnv1a64'): string
    {
        $type = gettype($value);
        switch ($type) {
            case 'object':
                if (Test::equatable($value)) {
                    $string = $value->hashValue();
                } else {
                    $string = sprintf('o_%s', spl_object_hash($value));
                }
                break;
            case 'string':
                $string = sprintf('s_%s', $value);
                break;
            case 'integer':
                $string = sprintf('i_%d', $value);
                break;
            case 'double':
                $string = sprintf('f_%.14F', $value);
                break;
            case 'boolean':
                $string = sprintf('b_%d', (int) $value);
                break;
            case 'resource':
                $string = sprintf('r_%d', (int) $value);
                break;
            case 'array':
                $string = sprintf('a_%s', serialize($value));
                break;
            default:
                $string = '0';
                break;
        }

        return hash($algo, $string);
    }
}
