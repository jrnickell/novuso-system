<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\ValueException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\ValueException
 */
class ValueExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Invalid value';
        $exception = new ValueException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new ValueException('Invalid value');
        $this->assertInstanceOf('Novuso\\System\\Exception\\SystemException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new ValueException('Invalid value');
        $this->assertSame(100, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new ValueException('Invalid value', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = ValueException::create('Invalid value');
        $this->assertInstanceOf('Novuso\\System\\Exception\\ValueException', $exception);
    }
}
