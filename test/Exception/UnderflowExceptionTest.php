<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\UnderflowException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\UnderflowException
 */
class UnderflowExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Stack underflow';
        $exception = new UnderflowException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new UnderflowException('Stack underflow');
        $this->assertInstanceOf('Novuso\\System\\Exception\\OperationException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new UnderflowException('Stack underflow');
        $this->assertSame(302, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new UnderflowException('Stack underflow', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = UnderflowException::create('Stack underflow');
        $this->assertInstanceOf('Novuso\\System\\Exception\\UnderflowException', $exception);
    }
}
