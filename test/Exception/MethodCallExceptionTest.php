<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\MethodCallException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\MethodCallException
 */
class MethodCallExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Invalid method call';
        $exception = new MethodCallException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new MethodCallException('Invalid method call');
        $this->assertInstanceOf('Novuso\\System\\Exception\\OperationException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new MethodCallException('Invalid method call');
        $this->assertSame(301, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new MethodCallException('Invalid method call', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = MethodCallException::create('Invalid method call');
        $this->assertInstanceOf('Novuso\\System\\Exception\\MethodCallException', $exception);
    }
}
