<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\RangeException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\RangeException
 */
class RangeExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Integer overflow';
        $exception = new RangeException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new RangeException('Integer overflow');
        $this->assertInstanceOf('Novuso\\System\\Exception\\SystemException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new RangeException('Integer overflow');
        $this->assertSame(3, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new RangeException('Integer overflow', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = RangeException::create('Integer overflow');
        $this->assertInstanceOf('Novuso\\System\\Exception\\RangeException', $exception);
    }
}
