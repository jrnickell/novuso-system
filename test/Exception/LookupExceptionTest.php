<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\LookupException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\LookupException
 */
class LookupExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Item not found';
        $exception = new LookupException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new LookupException('Item not found');
        $this->assertInstanceOf('Novuso\\System\\Exception\\SystemException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new LookupException('Item not found');
        $this->assertSame(200, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new LookupException('Item not found', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = LookupException::create('Item not found');
        $this->assertInstanceOf('Novuso\\System\\Exception\\LookupException', $exception);
    }
}
