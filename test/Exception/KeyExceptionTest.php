<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\KeyException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\KeyException
 */
class KeyExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Key not found';
        $exception = new KeyException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new KeyException('Key not found');
        $this->assertInstanceOf('Novuso\\System\\Exception\\LookupException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new KeyException('Key not found');
        $this->assertSame(202, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new KeyException('Key not found', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = KeyException::create('Key not found');
        $this->assertInstanceOf('Novuso\\System\\Exception\\KeyException', $exception);
    }
}
