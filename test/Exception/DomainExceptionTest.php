<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\DomainException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\DomainException
 */
class DomainExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Something else expected';
        $exception = new DomainException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new DomainException('Something else expected');
        $this->assertInstanceOf('Novuso\\System\\Exception\\ValueException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new DomainException('Something else expected');
        $this->assertSame(101, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new DomainException('Something else expected', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = DomainException::create('Something else expected');
        $this->assertInstanceOf('Novuso\\System\\Exception\\DomainException', $exception);
    }
}
