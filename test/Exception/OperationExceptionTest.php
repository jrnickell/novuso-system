<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\OperationException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\OperationException
 */
class OperationExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Invalid operation';
        $exception = new OperationException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new OperationException('Invalid operation');
        $this->assertInstanceOf('Novuso\\System\\Exception\\SystemException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new OperationException('Invalid operation');
        $this->assertSame(300, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new OperationException('Invalid operation', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = OperationException::create('Invalid operation');
        $this->assertInstanceOf('Novuso\\System\\Exception\\OperationException', $exception);
    }
}
