<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\IndexException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\IndexException
 */
class IndexExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Index out of bounds';
        $exception = new IndexException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new IndexException('Index out of bounds');
        $this->assertInstanceOf('Novuso\\System\\Exception\\LookupException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new IndexException('Index out of bounds');
        $this->assertSame(201, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new IndexException('Index out of bounds', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = IndexException::create('Index out of bounds');
        $this->assertInstanceOf('Novuso\\System\\Exception\\IndexException', $exception);
    }
}
