<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\SystemException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\SystemException
 */
class SystemExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Something went wrong';
        $exception = new SystemException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new SystemException('Something went wrong');
        $this->assertInstanceOf('Exception', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new SystemException('Something went wrong');
        $this->assertSame(1, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new SystemException('Something went wrong', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = SystemException::create('Something went wrong');
        $this->assertInstanceOf('Novuso\\System\\Exception\\SystemException', $exception);
    }
}
