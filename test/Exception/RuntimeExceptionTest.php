<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\RuntimeException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\RuntimeException
 */
class RuntimeExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Runtime error';
        $exception = new RuntimeException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new RuntimeException('Runtime error');
        $this->assertInstanceOf('Novuso\\System\\Exception\\SystemException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new RuntimeException('Runtime error');
        $this->assertSame(2, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new RuntimeException('Runtime error', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = RuntimeException::create('Runtime error');
        $this->assertInstanceOf('Novuso\\System\\Exception\\RuntimeException', $exception);
    }
}
