<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\TypeException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\TypeException
 */
class TypeExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Invalid argument type';
        $exception = new TypeException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new TypeException('Invalid argument type');
        $this->assertInstanceOf('Novuso\\System\\Exception\\ValueException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new TypeException('Invalid argument type');
        $this->assertSame(102, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new TypeException('Invalid argument type', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = TypeException::create('Invalid argument type');
        $this->assertInstanceOf('Novuso\\System\\Exception\\TypeException', $exception);
    }
}
