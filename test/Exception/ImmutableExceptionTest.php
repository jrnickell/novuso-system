<?php

namespace Novuso\Test\System\Exception;

use Novuso\System\Exception\ImmutableException;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Exception\ImmutableException
 */
class ImmutableExceptionTest extends PHPUnit_Framework_TestCase
{
    public function test_that_constructor_takes_message_as_argument()
    {
        $message = 'Cannot modify this';
        $exception = new ImmutableException($message);
        $this->assertSame($message, $exception->getMessage());
    }

    public function test_that_parent_exception_matches_expected()
    {
        $exception = new ImmutableException('Cannot modify this');
        $this->assertInstanceOf('Novuso\\System\\Exception\\OperationException', $exception);
    }

    public function test_that_default_code_matches_expected()
    {
        $exception = new ImmutableException('Cannot modify this');
        $this->assertSame(303, $exception->getCode());
    }

    public function test_that_default_code_can_be_overridden_in_constructor()
    {
        $exception = new ImmutableException('Cannot modify this', 1000);
        $this->assertSame(1000, $exception->getCode());
    }

    public function test_that_create_returns_exception_instance()
    {
        $exception = ImmutableException::create('Cannot modify this');
        $this->assertInstanceOf('Novuso\\System\\Exception\\ImmutableException', $exception);
    }
}
