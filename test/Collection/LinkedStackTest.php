<?php

namespace Novuso\Test\System\Collection;

use Novuso\System\Collection\LinkedStack;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Collection\LinkedStack
 */
class LinkedStackTest extends PHPUnit_Framework_TestCase
{
    public function test_that_it_is_empty_by_default()
    {
        $this->assertTrue(LinkedStack::of('int')->isEmpty());
    }

    public function test_that_adding_items_affects_count()
    {
        $stack = LinkedStack::of('int');
        foreach (range(0, 9) as $i) {
            $stack->push($i);
        }
        $this->assertCount(10, $stack);
    }

    public function test_that_pop_returns_expected_item()
    {
        $stack = LinkedStack::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $stack->push($i);
        }
        $output = [];
        foreach ($items as $i) {
            $output[] = $stack->pop();
        }
        $this->assertSame($items, array_reverse($output));
    }

    public function test_that_pop_returns_item_with_removal()
    {
        $stack = LinkedStack::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $stack->push($i);
        }
        $stack->pop();
        $this->assertCount(9, $stack);
    }

    public function test_that_top_returns_item_without_removal()
    {
        $stack = LinkedStack::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $stack->push($i);
        }
        $stack->top();
        $this->assertCount(10, $stack);
    }

    public function test_that_mixing_add_remove_operations_affects_order()
    {
        $stack = LinkedStack::of('int');
        $items = range(0, 99);
        foreach ($items as $i) {
            $stack->push($i);
            if ($i % 2 === 0) {
                $stack->pop();
            }
        }
        $remaining = [];
        for ($i = 0; $i < 50; $i++) {
            $remaining[] = $stack->pop();
        }
        $this->assertSame(range(1, 99, 2), array_reverse($remaining));
    }

    public function test_that_it_is_traversable()
    {
        $stack = LinkedStack::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $stack->push($i);
        }
        $output = [];
        foreach ($stack as $item) {
            $output[] = $item;
        }
        $this->assertSame($items, array_reverse($output));
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_push_triggers_assert_error_for_invalid_item_type()
    {
        LinkedStack::of('int')->push('string');
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_pop_throws_exception_when_empty()
    {
        LinkedStack::of('int')->pop();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_top_throws_exception_when_empty()
    {
        LinkedStack::of('int')->top();
    }
}
