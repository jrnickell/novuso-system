<?php

namespace Novuso\Test\System\Collection;

use Novuso\System\Collection\HashSet;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Collection\HashSet
 */
class HashSetTest extends PHPUnit_Framework_TestCase
{
    public function test_that_it_is_empty_by_default()
    {
        $this->assertTrue(HashSet::of('string')->isEmpty());
    }

    public function test_that_duplicate_items_do_not_affect_count()
    {
        $set = HashSet::of('string');
        $set->add('foo');
        $set->add('bar');
        $set->add('foo');
        $this->assertSame(2, count($set));
    }

    public function test_that_contains_returns_true_when_item_is_in_the_set()
    {
        $set = HashSet::of('string');
        $set->add('foo');
        $set->add('bar');
        $this->assertTrue($set->contains('bar'));
    }

    public function test_that_contains_returns_false_when_item_is_not_in_the_set()
    {
        $set = HashSet::of('string');
        $set->add('foo');
        $set->add('bar');
        $this->assertFalse($set->contains('baz'));
    }

    public function test_that_contains_returns_false_after_item_is_removed()
    {
        $set = HashSet::of('string');
        $set->add('foo');
        $set->add('bar');
        $set->remove('foo');
        $this->assertFalse($set->contains('foo'));
    }

    public function test_that_difference_returns_empty_set_from_same_instances()
    {
        $twos = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30];
        $set = HashSet::of('int');
        foreach ($twos as $val) {
            $set->add($val);
        }
        $difference = $set->difference($set);
        $this->assertTrue($difference->isEmpty());
    }

    public function test_that_difference_returns_expected_set()
    {
        $twos = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30];
        $threes = [3, 6, 9, 12, 15, 18, 21, 24, 30];
        $setOfTwos = HashSet::of('int');
        $setOfThrees = HashSet::of('int');
        foreach ($twos as $val) {
            $setOfTwos->add($val);
        }
        foreach ($threes as $val) {
            $setOfThrees->add($val);
        }
        $validSet = [2, 3, 4, 8, 9, 10, 14, 15, 16, 20, 21, 22, 26, 28];
        $invalidSet = [6, 12, 18, 24, 30];
        $difference = $setOfTwos->difference($setOfThrees);
        $valid = true;
        foreach ($validSet as $val) {
            if (!$difference->contains($val)) {
                $valid = false;
            }
        }
        foreach ($invalidSet as $val) {
            if ($difference->contains($val)) {
                $value = false;
            }
        }
        $this->assertTrue($valid);
    }

    public function test_that_intersection_returns_expected_set()
    {
        $twos = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30];
        $threes = [3, 6, 9, 12, 15, 18, 21, 24, 30];
        $setOfTwos = HashSet::of('int');
        $setOfThrees = HashSet::of('int');
        foreach ($twos as $val) {
            $setOfTwos->add($val);
        }
        foreach ($threes as $val) {
            $setOfThrees->add($val);
        }
        $validSet = [6, 12, 18, 24, 30];
        $invalidSet = [2, 3, 4, 8, 9, 10, 14, 15, 16, 20, 21, 22, 26, 28];
        $intersection = $setOfTwos->intersection($setOfThrees);
        $valid = true;
        foreach ($validSet as $val) {
            if (!$intersection->contains($val)) {
                $valid = false;
            }
        }
        foreach ($invalidSet as $val) {
            if ($intersection->contains($val)) {
                $value = false;
            }
        }
        $this->assertTrue($valid);
    }

    public function test_that_complement_returns_empty_set_from_same_instances()
    {
        $twos = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30];
        $set = HashSet::of('int');
        foreach ($twos as $val) {
            $set->add($val);
        }
        $complement = $set->complement($set);
        $this->assertTrue($complement->isEmpty());
    }

    public function test_that_complement_returns_expected_set()
    {
        $twos = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30];
        $threes = [3, 6, 9, 12, 15, 18, 21, 24, 30];
        $setOfTwos = HashSet::of('int');
        $setOfThrees = HashSet::of('int');
        foreach ($twos as $val) {
            $setOfTwos->add($val);
        }
        foreach ($threes as $val) {
            $setOfThrees->add($val);
        }
        $validSet = [3, 9, 15, 21];
        $invalidSet = [6, 12, 18, 24, 30];
        $complement = $setOfTwos->complement($setOfThrees);
        $valid = true;
        foreach ($validSet as $val) {
            if (!$complement->contains($val)) {
                $valid = false;
            }
        }
        foreach ($invalidSet as $val) {
            if ($complement->contains($val)) {
                $value = false;
            }
        }
        $this->assertTrue($valid);
    }

    public function test_that_union_returns_expected_set()
    {
        $twos = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30];
        $threes = [3, 6, 9, 12, 15, 18, 21, 24, 30];
        $setOfTwos = HashSet::of('int');
        $setOfThrees = HashSet::of('int');
        foreach ($twos as $val) {
            $setOfTwos->add($val);
        }
        foreach ($threes as $val) {
            $setOfThrees->add($val);
        }
        $validSet = [2, 3, 4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 26, 28, 30];
        $invalidSet = [1, 5, 7, 11, 13, 17, 19, 23, 25, 27, 29];
        $union = $setOfTwos->union($setOfThrees);
        $valid = true;
        foreach ($validSet as $val) {
            if (!$union->contains($val)) {
                $valid = false;
            }
        }
        foreach ($invalidSet as $val) {
            if ($union->contains($val)) {
                $value = false;
            }
        }
        $this->assertTrue($valid);
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_add_triggers_assert_error_for_invalid_item_type()
    {
        HashSet::of('int')->add('string');
    }
}
