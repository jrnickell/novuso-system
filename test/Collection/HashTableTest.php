<?php

namespace Novuso\Test\System\Collection;

use Novuso\System\Collection\HashTable;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Collection\HashTable
 * @covers Novuso\System\Collection\Traits\KeyValueTypeMethods
 */
class HashTableTest extends PHPUnit_Framework_TestCase
{
    public function test_that_it_is_empty_by_default()
    {
        $this->assertTrue(HashTable::of('string', 'string')->isEmpty());
    }

    public function test_that_duplicate_keys_do_not_affect_count()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable->set('foo', 'bar');
        $hashTable->set('baz', 'buz');
        $hashTable->set('foo', 'bar');
        $this->assertSame(2, count($hashTable));
    }

    public function test_that_get_returns_expected_value_for_key()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable->set('foo', 'bar');
        $hashTable->set('baz', 'buz');
        $this->assertSame('bar', $hashTable->get('foo'));
    }

    public function test_that_has_returns_true_when_key_is_in_the_table()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable->set('foo', 'bar');
        $this->assertTrue($hashTable->has('foo'));
    }

    public function test_that_has_returns_false_when_key_is_not_in_the_table()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable->set('foo', 'bar');
        $this->assertFalse($hashTable->has('baz'));
    }

    public function test_that_has_returns_false_after_key_is_removed()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable->set('foo', 'bar');
        $hashTable->remove('foo');
        $this->assertFalse($hashTable->has('foo'));
    }

    public function test_that_offset_get_returns_expected_value_for_key()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable['foo'] = 'bar';
        $hashTable['baz'] = 'buz';
        $this->assertSame('bar', $hashTable['foo']);
    }

    public function test_that_offset_exists_returns_true_when_key_is_in_the_table()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable['foo'] = 'bar';
        $this->assertTrue(isset($hashTable['foo']));
    }

    public function test_that_offset_exists_returns_false_when_key_is_not_in_the_table()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable['foo'] = 'bar';
        $this->assertFalse(isset($hashTable['baz']));
    }

    public function test_that_offset_exists_returns_false_after_key_is_removed()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable['foo'] = 'bar';
        unset($hashTable['foo']);
        $this->assertFalse(isset($hashTable['foo']));
    }

    public function test_that_keys_returns_traversable_list_of_keys()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable->set('foo', 'bar');
        $hashTable->set('baz', 'buz');
        $keys = $hashTable->keys();
        $output = [];
        foreach ($keys as $key) {
            $output[] = $key;
        }
        $this->assertContains('foo', $output);
    }

    public function test_that_it_is_traversable()
    {
        $hashTable = HashTable::of('string', 'string');
        $hashTable->set('foo', 'bar');
        $hashTable->set('baz', 'buz');
        foreach ($hashTable as $key => $value) {
            if ($key === 'baz') {
                $this->assertSame('buz', $value);
            }
        }
    }

    public function test_that_iterator_key_returns_null_when_invalid()
    {
        $hashTable = HashTable::of('string', 'string');
        $this->assertNull($hashTable->getIterator()->key());
    }

    public function test_that_iterator_current_returns_null_when_invalid()
    {
        $hashTable = HashTable::of('string', 'string');
        $this->assertNull($hashTable->getIterator()->current());
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_set_triggers_assert_error_for_invalid_key_type()
    {
        HashTable::of('string', 'string')->set(10, 'foo');
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_set_triggers_assert_error_for_invalid_value_type()
    {
        HashTable::of('string', 'string')->set('foo', 10);
    }

    /**
     * @expectedException Novuso\System\Exception\KeyException
     */
    public function test_that_get_throws_exception_for_key_not_found()
    {
        HashTable::of('string', 'string')->get('foo');
    }
}
