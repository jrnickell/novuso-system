<?php

namespace Novuso\Test\System\Collection;

use Novuso\System\Collection\LinkedDeque;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Collection\LinkedDeque
 */
class LinkedDequeTest extends PHPUnit_Framework_TestCase
{
    public function test_that_it_is_empty_by_default()
    {
        $this->assertTrue(LinkedDeque::of('int')->isEmpty());
    }

    public function test_that_adding_items_affects_count()
    {
        $deque = LinkedDeque::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $deque->addLast($i);
        }
        $this->assertCount(10, $deque);
    }

    public function test_that_remove_first_returns_expected_item()
    {
        $deque = LinkedDeque::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $deque->addLast($i);
        }
        $output = [];
        foreach ($items as $i) {
            $output[] = $deque->removeFirst();
        }
        $this->assertSame($items, $output);
    }

    public function test_that_remove_last_returns_expected_item()
    {
        $deque = LinkedDeque::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $deque->addFirst($i);
        }
        $output = [];
        foreach ($items as $i) {
            $output[] = $deque->removeLast();
        }
        $this->assertSame($items, $output);
    }

    public function test_that_remove_first_returns_item_with_removal()
    {
        $deque = LinkedDeque::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $deque->addLast($i);
        }
        $deque->removeFirst();
        $this->assertCount(9, $deque);
    }

    public function test_that_remove_last_returns_item_with_removal()
    {
        $deque = LinkedDeque::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $deque->addFirst($i);
        }
        $deque->removeLast();
        $this->assertCount(9, $deque);
    }

    public function test_that_first_returns_item_without_removal()
    {
        $deque = LinkedDeque::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $deque->addLast($i);
        }
        $deque->first();
        $this->assertCount(10, $deque);
    }

    public function test_that_last_returns_item_without_removal()
    {
        $deque = LinkedDeque::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $deque->addFirst($i);
        }
        $deque->last();
        $this->assertCount(10, $deque);
    }

    public function test_that_it_is_traversable()
    {
        $deque = LinkedDeque::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $deque->addLast($i);
        }
        $output = [];
        foreach ($deque as $item) {
            $output[] = $item;
        }
        $this->assertSame($items, $output);
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_remove_first_throws_exception_when_empty()
    {
        LinkedDeque::of('int')->removeFirst();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_remove_last_throws_exception_when_empty()
    {
        LinkedDeque::of('int')->removeLast();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_first_throws_exception_when_empty()
    {
        LinkedDeque::of('int')->first();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_last_throws_exception_when_empty()
    {
        LinkedDeque::of('int')->last();
    }
}
