<?php

namespace Novuso\Test\System\Collection;

use Novuso\System\Collection\SortedTable;
use Novuso\Test\System\Doubles\WeekDay;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Collection\SortedTable
 */
class SortedTableTest extends PHPUnit_Framework_TestCase
{
    public function test_that_it_is_empty_by_default()
    {
        $this->assertTrue(SortedTable::of(WeekDay::class, 'string')->isEmpty());
    }

    public function test_that_adding_items_affects_count()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $this->assertCount(7, $table);
    }

    public function test_that_duplicate_keys_are_overridden()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        $table->set(WeekDay::TUESDAY(), 'Tacos');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $this->assertEquals('Tuesday', $table->get(WeekDay::TUESDAY()));
    }

    public function test_that_has_returns_true_for_valid_key()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $this->assertTrue($table->has(WeekDay::SATURDAY()));
    }

    public function test_that_remove_correctly_finds_and_removes()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        foreach ($this->getWeekDays() as $value => $key) {
            $table->remove($key);
        }
        $this->assertTrue($table->isEmpty());
    }

    public function test_that_offset_set_accepts_comparable_keys()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table[$key] = $value;
        }
        $this->assertSame('Monday', $table[WeekDay::MONDAY()]);
    }

    public function test_that_offset_exists_returns_true_for_valid_key()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table[$key] = $value;
        }
        $this->assertTrue(isset($table[WeekDay::SATURDAY()]));
    }

    public function test_that_offset_unset_correctly_finds_and_removes()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table[$key] = $value;
        }
        foreach ($this->getWeekDays() as $value => $key) {
            unset($table[$key]);
        }
        $this->assertTrue($table->isEmpty());
    }

    public function test_that_keys_returns_empty_traversable_when_empty()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        $keys = $table->keys();
        $count = 0;
        foreach ($keys as $key) {
            $count++;
        }
        $this->assertSame(0, $count);
    }

    public function test_that_keys_returns_traversable_keys_in_order()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $count = 0;
        foreach ($table->keys() as $key) {
            if ($key->value() !== $count) {
                throw new \Exception('Keys out of order');
            }
            $count++;
        }
        $this->assertSame(7, $count);
    }

    public function test_that_range_keys_returns_inclusive_set()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $keys = $table->rangeKeys(WeekDay::TUESDAY(), WeekDay::THURSDAY());
        $count = 0;
        foreach ($keys as $key) {
            $count++;
        }
        $this->assertSame(3, $count);
    }

    public function test_that_range_count_includes_key_arguments_when_present()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $count = $table->rangeCount(WeekDay::TUESDAY(), WeekDay::THURSDAY());
        $this->assertSame(3, $count);
    }

    public function test_that_range_count_does_not_include_key_arguments_when_missing()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $table->remove(WeekDay::THURSDAY());
        $count = $table->rangeCount(WeekDay::TUESDAY(), WeekDay::THURSDAY());
        $this->assertSame(2, $count);
    }

    public function test_that_range_count_returns_zero_for_args_out_of_order()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $count = $table->rangeCount(WeekDay::THURSDAY(), WeekDay::TUESDAY());
        $this->assertSame(0, $count);
    }

    public function test_that_remove_min_correctly_finds_and_removes()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        for ($i = 0; $i < 6; $i++) {
            $table->removeMin();
        }
        $this->assertSame('Saturday', $table->get(WeekDay::SATURDAY()));
    }

    public function test_that_remove_max_correctly_finds_and_removes()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        for ($i = 0; $i < 6; $i++) {
            $table->removeMax();
        }
        $this->assertSame('Sunday', $table->get(WeekDay::SUNDAY()));
    }

    public function test_that_floor_returns_equal_key_when_present()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $this->assertTrue(WeekDay::FRIDAY()->equals($table->floor(WeekDay::FRIDAY())));
    }

    public function test_that_floor_returns_largest_key_equal_or_less_than_arg()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $table->remove(WeekDay::THURSDAY());
        $this->assertTrue(WeekDay::WEDNESDAY()->equals($table->floor(WeekDay::THURSDAY())));
    }

    public function test_that_floor_returns_null_when_equal_or_less_key_not_found()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $table->remove(WeekDay::SUNDAY());
        $this->assertNull($table->floor(WeekDay::SUNDAY()));
    }

    public function test_that_ceiling_returns_equal_key_when_present()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $this->assertTrue(WeekDay::MONDAY()->equals($table->ceiling(WeekDay::MONDAY())));
    }

    public function test_that_ceiling_returns_smallest_key_equal_or_greater_than_arg()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $table->remove(WeekDay::TUESDAY());
        $this->assertTrue(WeekDay::WEDNESDAY()->equals($table->ceiling(WeekDay::TUESDAY())));
    }

    public function test_that_ceiling_returns_null_when_equal_or_greater_key_not_found()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $table->remove(WeekDay::SATURDAY());
        $this->assertNull($table->ceiling(WeekDay::SATURDAY()));
    }

    public function test_that_index_returns_expected_value_for_key()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $this->assertSame(4, $table->index(WeekDay::THURSDAY()));
    }

    public function test_that_select_returns_key_assoc_with_index()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $this->assertTrue(WeekDay::THURSDAY()->equals($table->select(4)));
    }

    public function test_that_it_is_traversable()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $count = 0;
        foreach ($table as $key => $value) {
            if ($key->value() !== $count) {
                throw new \Exception('Keys out of order');
            }
            $count++;
        }
        $this->assertSame(7, $count);
    }

    public function test_that_iterator_returns_null_for_invalid_key()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $iterator = $table->getIterator();
        foreach ($iterator as $key => $value) {
            //
        }
        $this->assertNull($iterator->key());
    }

    public function test_that_iterator_returns_null_for_invalid_value()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        foreach ($this->getWeekDays() as $value => $key) {
            $table->set($key, $value);
        }
        $iterator = $table->getIterator();
        foreach ($iterator as $key => $value) {
            //
        }
        $this->assertNull($iterator->current());
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_offset_set_throws_exception_for_non_comparable_key()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        $table['foo'] = 'bar';
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_offset_get_throws_exception_for_non_comparable_key()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        $table['foo'];
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_offset_exists_throws_exception_for_non_comparable_key()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        isset($table['foo']);
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_offset_unset_throws_exception_for_non_comparable_key()
    {
        $table = SortedTable::of(WeekDay::class, 'string');
        unset($table['foo']);
    }

    /**
     * @expectedException Novuso\System\Exception\KeyException
     */
    public function test_that_get_throws_exception_for_undefined_key()
    {
        SortedTable::of(WeekDay::class, 'string')->get(WeekDay::SUNDAY());
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_min_throws_exception_when_empty()
    {
        SortedTable::of(WeekDay::class, 'string')->min();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_max_throws_exception_when_empty()
    {
        SortedTable::of(WeekDay::class, 'string')->max();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_remove_min_throws_exception_when_empty()
    {
        SortedTable::of(WeekDay::class, 'string')->removeMin();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_remove_max_throws_exception_when_empty()
    {
        SortedTable::of(WeekDay::class, 'string')->removeMax();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_floor_throws_exception_when_empty()
    {
        SortedTable::of(WeekDay::class, 'string')->floor(WeekDay::WEDNESDAY());
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_ceiling_throws_exception_when_empty()
    {
        SortedTable::of(WeekDay::class, 'string')->ceiling(WeekDay::WEDNESDAY());
    }

    /**
     * @expectedException Novuso\System\Exception\IndexException
     */
    public function test_that_select_throws_exception_when_index_out_of_bounds()
    {
        SortedTable::of(WeekDay::class, 'string')->select(10);
    }

    protected function getWeekDays()
    {
        return [
            'Monday'    => WeekDay::MONDAY(),
            'Wednesday' => WeekDay::WEDNESDAY(),
            'Friday'    => WeekDay::FRIDAY(),
            'Tuesday'   => WeekDay::TUESDAY(),
            'Thursday'  => WeekDay::THURSDAY(),
            'Saturday'  => WeekDay::SATURDAY(),
            'Sunday'    => WeekDay::SUNDAY()
        ];
    }
}
