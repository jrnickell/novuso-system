<?php

namespace Novuso\Test\System\Collection;

use Novuso\System\Collection\ArrayList;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Collection\ArrayList
 * @covers Novuso\System\Collection\Traits\ItemTypeMethods
 */
class ArrayListTest extends PHPUnit_Framework_TestCase
{
    public function test_that_it_is_empty_by_default()
    {
        $this->assertTrue(ArrayList::of('string')->isEmpty());
    }

    public function test_that_added_items_affect_count()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->add('foo');
        $this->assertSame(3, count($list));
    }

    public function test_that_set_replaces_item_at_an_index()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->set(1, 'baz');
        $this->assertSame(2, count($list));
    }

    public function test_that_set_replaces_item_at_a_neg_index()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->set(-1, 'baz');
        $this->assertSame('baz', $list->get(1));
    }

    public function test_that_get_returns_item_at_pos_index()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->add('baz');
        $this->assertSame('bar', $list->get(1));
    }

    public function test_that_get_returns_item_at_neg_index()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->add('baz');
        $this->assertSame('bar', $list->get(-2));
    }

    public function test_that_has_returns_true_for_index_in_bounds()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->add('baz');
        $this->assertTrue($list->has(2));
    }

    public function test_that_has_returns_false_for_index_out_of_bounds()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->add('baz');
        $this->assertFalse($list->has(4));
    }

    public function test_that_remove_deletes_item_and_reindexes_list()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->add('baz');
        $list->remove(1);
        $this->assertSame('baz', $list->get(1));
    }

    public function test_that_remove_deletes_correct_item_at_neg_index()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->add('baz');
        $list->remove(-2);
        $this->assertSame('baz', $list->get(1));
    }

    public function test_that_remove_fails_silently_at_out_of_bounds_index()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list->add('bar');
        $list->add('baz');
        $list->remove(4);
        $this->assertSame(3, count($list));
    }

    public function test_that_offset_set_adds_item_when_used_without_index()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $this->assertSame('bar', $list[1]);
    }

    public function test_that_offset_set_replaces_item_at_given_index()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $list[1] = 'baz';
        $this->assertSame(2, count($list));
    }

    public function test_that_offset_exists_returns_true_for_index_in_bounds()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $this->assertTrue(isset($list[1]));
    }

    public function test_that_offset_exists_returns_false_for_index_out_of_bounds()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $this->assertFalse(isset($list[2]));
    }

    public function test_that_offset_unset_deletes_item_and_reindexes_list()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $list[] = 'baz';
        unset($list[1]);
        $this->assertSame('baz', $list[1]);
    }

    public function test_that_first_returns_expected_item()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $list[] = 'baz';
        $this->assertSame('foo', $list->first());
    }

    public function test_that_last_returns_expected_item()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $list[] = 'baz';
        $this->assertSame('baz', $list->last());
    }

    public function test_that_it_is_iterable_forward()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $list[] = 'baz';
        for ($list->rewind(); $list->valid(); $list->next()) {
            if ($list->key() === 1) {
                $this->assertSame('bar', $list->current());
            }
        }
    }

    public function test_that_it_is_iterable_in_reverse()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $list[] = 'baz';
        for ($list->end(); $list->valid(); $list->prev()) {
            if ($list->key() === 1) {
                $this->assertSame('bar', $list->current());
            }
        }
    }

    public function test_that_it_is_directly_traversable()
    {
        $list = ArrayList::of('string');
        $list[] = 'foo';
        $list[] = 'bar';
        $list[] = 'baz';
        foreach ($list as $index => $item) {
            if ($index === 1) {
                $this->assertSame('bar', $item);
            }
        }
    }

    public function test_that_calling_key_without_valid_item_returns_null()
    {
        $list = ArrayList::of('string');
        $this->assertNull($list->key());
    }

    public function test_that_calling_current_without_valid_item_returns_null()
    {
        $list = ArrayList::of('string');
        $this->assertNull($list->current());
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_add_triggers_assert_error_for_invalid_item_type()
    {
        ArrayList::of('object')->add('string');
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_set_triggers_assert_error_for_invalid_item_type()
    {
        $list = ArrayList::of('object');
        $list->add(new \stdClass());
        $list->set(0, 'string');
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_offset_set_triggers_assert_error_for_invalid_index_type()
    {
        $list = ArrayList::of('string');
        $list['foo'] = 'bar';
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_offset_get_triggers_assert_error_for_invalid_index_type()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        $list['foo'];
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_offset_exists_triggers_assert_error_for_invalid_index_type()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        isset($list['foo']);
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_offset_unset_triggers_assert_error_for_invalid_index_type()
    {
        $list = ArrayList::of('string');
        $list->add('foo');
        unset($list['foo']);
    }

    /**
     * @expectedException Novuso\System\Exception\IndexException
     */
    public function test_that_set_throws_exception_for_invalid_index()
    {
        $list = ArrayList::of('object');
        $list->add(new \stdClass());
        $list->set(1, new \stdClass());
    }

    /**
     * @expectedException Novuso\System\Exception\IndexException
     */
    public function test_that_get_throws_exception_for_invalid_index()
    {
        $list = ArrayList::of('object');
        $list->add(new \stdClass());
        $list->get(1);
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_first_throws_exception_when_empty()
    {
        $list = ArrayList::of('string');
        $list->first();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_last_throws_exception_when_empty()
    {
        $list = ArrayList::of('string');
        $list->last();
    }
}
