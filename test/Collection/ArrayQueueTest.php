<?php

namespace Novuso\Test\System\Collection;

use Novuso\System\Collection\ArrayQueue;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Collection\ArrayQueue
 */
class ArrayQueueTest extends PHPUnit_Framework_TestCase
{
    public function test_that_it_is_empty_by_default()
    {
        $this->assertTrue(ArrayQueue::of('int')->isEmpty());
    }

    public function test_that_adding_items_affects_count()
    {
        $queue = ArrayQueue::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $queue->enqueue($i);
        }
        $this->assertCount(10, $queue);
    }

    public function test_that_dequeue_returns_expected_item()
    {
        $queue = ArrayQueue::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $queue->enqueue($i);
        }
        $output = [];
        foreach ($items as $i) {
            $output[] = $queue->dequeue();
        }
        $this->assertSame($items, $output);
    }

    public function test_that_dequeue_returns_item_with_removal()
    {
        $queue = ArrayQueue::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $queue->enqueue($i);
        }
        $queue->dequeue();
        $this->assertCount(9, $queue);
    }

    public function test_that_front_returns_item_without_removal()
    {
        $queue = ArrayQueue::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $queue->enqueue($i);
        }
        $queue->front();
        $this->assertCount(10, $queue);
    }

    public function test_that_mixing_add_remove_operations_keeps_order()
    {
        $queue = ArrayQueue::of('int');
        $items = range(0, 99);
        foreach ($items as $i) {
            $queue->enqueue($i);
            if ($i % 2 === 0) {
                $queue->dequeue();
            }
        }
        $remaining = [];
        for ($i = 0; $i < 50; $i++) {
            $remaining[] = $queue->dequeue();
        }
        $this->assertSame(range(50, 99), $remaining);
    }

    public function test_that_it_is_traversable()
    {
        $queue = ArrayQueue::of('int');
        $items = range(0, 9);
        foreach ($items as $i) {
            $queue->enqueue($i);
        }
        $output = [];
        foreach ($queue as $item) {
            $output[] = $item;
        }
        $this->assertSame($items, $output);
    }

    /**
     * @expectedException AssertionError
     */
    public function test_that_enqueue_triggers_assert_error_for_invalid_item_type()
    {
        ArrayQueue::of('int')->enqueue('string');
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_dequeue_throws_exception_when_empty()
    {
        ArrayQueue::of('int')->dequeue();
    }

    /**
     * @expectedException Novuso\System\Exception\UnderflowException
     */
    public function test_that_front_throws_exception_when_empty()
    {
        ArrayQueue::of('int')->front();
    }
}
