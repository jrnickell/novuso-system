<?php

namespace Novuso\Test\System\Utility;

use Novuso\System\Utility\Hasher;
use Novuso\Test\System\Doubles\StringObject;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Utility\Hasher
 */
class HasherTest extends PHPUnit_Framework_TestCase
{
    const HASH_ALGO = 'fnv1a64';

    public function test_that_hash_returns_expected_value_for_null()
    {
        $expected = 'af63ad4c86019caf';
        $this->assertSame($expected, Hasher::hash(null));
    }

    public function test_that_hash_returns_expected_value_for_true()
    {
        $expected = '00f0771913d4c24d';
        $this->assertSame($expected, Hasher::hash(true));
    }

    public function test_that_hash_returns_expected_value_for_false()
    {
        $expected = '00f0761913d4c09a';
        $this->assertSame($expected, Hasher::hash(false));
    }

    public function test_that_hash_returns_expected_value_for_std_class()
    {
        $object = new \StdClass();
        $objHash = spl_object_hash($object);
        $expected = hash(static::HASH_ALGO, sprintf('o_%s', $objHash));
        $this->assertSame($expected, Hasher::hash($object));
    }

    public function test_that_hash_returns_expected_value_for_equatable()
    {
        $string = 'Hello World';
        $expected = hash(static::HASH_ALGO, $string);
        $this->assertSame($expected, Hasher::hash(new StringObject($string)));
    }

    public function test_that_hash_returns_expected_value_for_string()
    {
        $expected = 'f3b2b5df97295c95';
        $this->assertSame($expected, Hasher::hash('Hello World'));
    }

    public function test_that_hash_returns_expected_value_for_integer()
    {
        $expected = '9e9f5ec66b6bcca7';
        $this->assertSame($expected, Hasher::hash(42));
    }

    public function test_that_hash_returns_expected_value_for_float()
    {
        $expected = 'b31295a8809317e2';
        $this->assertSame($expected, Hasher::hash(3.14));
    }

    public function test_that_hash_returns_expected_value_for_resource()
    {
        $handle = fopen(__FILE__, 'r');
        $expected = hash(static::HASH_ALGO, sprintf('r_%d', (int) $handle));
        $this->assertSame($expected, Hasher::hash($handle));
        fclose($handle);
    }

    public function test_that_hash_returns_expected_value_for_array()
    {
        $expected = 'b4d465eddf528273';
        $this->assertSame($expected, Hasher::hash(['foo', 'bar', 'baz']));
    }
}
