<?php

namespace Novuso\Test\System\Utility;

use Novuso\System\Utility\ClassName;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Utility\ClassName
 */
class ClassNameTest extends PHPUnit_Framework_TestCase
{
    public function test_that_full_returns_fqcn_when_passed_fqcn()
    {
        $className = 'Novuso\\System\\Exception\\TypeException';
        $this->assertSame($className, ClassName::full($className));
    }

    public function test_that_full_returns_fqcn_when_passed_object()
    {
        $className = static::class;
        $this->assertSame($className, ClassName::full($this));
    }

    public function test_that_canonical_returns_expected_value()
    {
        $expected = 'Novuso.System.Exception.TypeException';
        $className = 'Novuso\\System\\Exception\\TypeException';
        $this->assertSame($expected, ClassName::canonical($className));
    }

    public function test_that_underscore_returns_expected_value()
    {
        $expected = 'novuso.system.exception.type_exception';
        $className = 'Novuso\\System\\Exception\\TypeException';
        $this->assertSame($expected, ClassName::underscore($className));
    }

    public function test_that_short_returns_expected_value()
    {
        $expected = 'TypeException';
        $className = 'Novuso\\System\\Exception\\TypeException';
        $this->assertSame($expected, ClassName::short($className));
    }
}
