<?php

namespace Novuso\Test\System\Utility;

use Novuso\System\Utility\Test;
use PHPUnit_Framework_TestCase;

/**
 * @covers Novuso\System\Utility\Test
 */
class TestTest extends PHPUnit_Framework_TestCase
{
    use TestDataProvider;

    /**
     * @dataProvider validScalarProvider
     */
    public function test_that_scalar_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::scalar($value));
    }

    /**
     * @dataProvider invalidScalarProvider
     */
    public function test_that_scalar_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::scalar($value));
    }

    /**
     * @dataProvider validBoolProvider
     */
    public function test_that_bool_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::bool($value));
    }

    /**
     * @dataProvider invalidBoolProvider
     */
    public function test_that_bool_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::bool($value));
    }

    /**
     * @dataProvider validFloatProvider
     */
    public function test_that_float_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::float($value));
    }

    /**
     * @dataProvider invalidFloatProvider
     */
    public function test_that_float_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::float($value));
    }

    /**
     * @dataProvider validIntProvider
     */
    public function test_that_int_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::int($value));
    }

    /**
     * @dataProvider invalidIntProvider
     */
    public function test_that_int_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::int($value));
    }

    /**
     * @dataProvider validStringProvider
     */
    public function test_that_string_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::string($value));
    }

    /**
     * @dataProvider invalidStringProvider
     */
    public function test_that_string_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::string($value));
    }

    /**
     * @dataProvider validArrayProvider
     */
    public function test_that_array_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::array($value));
    }

    /**
     * @dataProvider invalidArrayProvider
     */
    public function test_that_array_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::array($value));
    }

    /**
     * @dataProvider validObjectProvider
     */
    public function test_that_object_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::object($value));
    }

    /**
     * @dataProvider invalidObjectProvider
     */
    public function test_that_object_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::object($value));
    }

    /**
     * @dataProvider validCallableProvider
     */
    public function test_that_callable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::callable($value));
    }

    /**
     * @dataProvider invalidCallableProvider
     */
    public function test_that_callable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::callable($value));
    }

    /**
     * @dataProvider validNullProvider
     */
    public function test_that_null_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::null($value));
    }

    /**
     * @dataProvider invalidNullProvider
     */
    public function test_that_null_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::null($value));
    }

    /**
     * @dataProvider validNotNullProvider
     */
    public function test_that_not_null_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::notNull($value));
    }

    /**
     * @dataProvider invalidNotNullProvider
     */
    public function test_that_not_null_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::notNull($value));
    }

    /**
     * @dataProvider validTrueProvider
     */
    public function test_that_true_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::true($value));
    }

    /**
     * @dataProvider invalidTrueProvider
     */
    public function test_that_true_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::true($value));
    }

    /**
     * @dataProvider validFalseProvider
     */
    public function test_that_false_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::false($value));
    }

    /**
     * @dataProvider invalidFalseProvider
     */
    public function test_that_false_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::false($value));
    }

    /**
     * @dataProvider validEmptyProvider
     */
    public function test_that_empty_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::empty($value));
    }

    /**
     * @dataProvider invalidEmptyProvider
     */
    public function test_that_empty_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::empty($value));
    }

    /**
     * @dataProvider validNotEmptyProvider
     */
    public function test_that_not_empty_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::notEmpty($value));
    }

    /**
     * @dataProvider invalidNotEmptyProvider
     */
    public function test_that_not_empty_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::notEmpty($value));
    }

    /**
     * @dataProvider validBlankProvider
     */
    public function test_that_blank_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::blank($value));
    }

    /**
     * @dataProvider invalidBlankProvider
     */
    public function test_that_blank_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::blank($value));
    }

    /**
     * @dataProvider validNotBlankProvider
     */
    public function test_that_not_blank_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::notBlank($value));
    }

    /**
     * @dataProvider invalidNotBlankProvider
     */
    public function test_that_not_blank_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::notBlank($value));
    }

    /**
     * @dataProvider validAlphaProvider
     */
    public function test_that_alpha_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::alpha($value));
    }

    /**
     * @dataProvider invalidAlphaProvider
     */
    public function test_that_alpha_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::alpha($value));
    }

    /**
     * @dataProvider validAlnumProvider
     */
    public function test_that_alnum_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::alnum($value));
    }

    /**
     * @dataProvider invalidAlnumProvider
     */
    public function test_that_alnum_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::alnum($value));
    }

    /**
     * @dataProvider validAlphaDashProvider
     */
    public function test_that_alpha_dash_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::alphaDash($value));
    }

    /**
     * @dataProvider invalidAlphaDashProvider
     */
    public function test_that_alpha_dash_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::alphaDash($value));
    }

    /**
     * @dataProvider validAlnumDashProvider
     */
    public function test_that_alnum_dash_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::alnumDash($value));
    }

    /**
     * @dataProvider invalidAlnumDashProvider
     */
    public function test_that_alnum_dash_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::alnumDash($value));
    }

    /**
     * @dataProvider validDigitsProvider
     */
    public function test_that_digits_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::digits($value));
    }

    /**
     * @dataProvider invalidDigitsProvider
     */
    public function test_that_digits_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::digits($value));
    }

    /**
     * @dataProvider validNumericProvider
     */
    public function test_that_numeric_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::numeric($value));
    }

    /**
     * @dataProvider invalidNumericProvider
     */
    public function test_that_numeric_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::numeric($value));
    }

    /**
     * @dataProvider validEmailProvider
     */
    public function test_that_email_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::email($value));
    }

    /**
     * @dataProvider invalidEmailProvider
     */
    public function test_that_email_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::email($value));
    }

    /**
     * @dataProvider validIpAddressProvider
     */
    public function test_that_ip_address_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::ipAddress($value));
    }

    /**
     * @dataProvider invalidIpAddressProvider
     */
    public function test_that_ip_address_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::ipAddress($value));
    }

    /**
     * @dataProvider validIpV4AddressProvider
     */
    public function test_that_ip_v4_address_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::ipV4Address($value));
    }

    /**
     * @dataProvider invalidIpV4AddressProvider
     */
    public function test_that_ip_v4_address_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::ipV4Address($value));
    }

    /**
     * @dataProvider validIpV6AddressProvider
     */
    public function test_that_ip_v6_address_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::ipV6Address($value));
    }

    /**
     * @dataProvider invalidIpV6AddressProvider
     */
    public function test_that_ip_v6_address_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::ipV6Address($value));
    }

    /**
     * @dataProvider validUriProvider
     */
    public function test_that_uri_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::uri($value));
    }

    /**
     * @dataProvider invalidUriProvider
     */
    public function test_that_uri_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::uri($value));
    }

    /**
     * @dataProvider validUrnProvider
     */
    public function test_that_urn_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::urn($value));
    }

    /**
     * @dataProvider invalidUrnProvider
     */
    public function test_that_urn_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::urn($value));
    }

    /**
     * @dataProvider validUuidProvider
     */
    public function test_that_uuid_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::uuid($value));
    }

    /**
     * @dataProvider invalidUuidProvider
     */
    public function test_that_uuid_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::uuid($value));
    }

    /**
     * @dataProvider validTimezoneProvider
     */
    public function test_that_timezone_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::timezone($value));
    }

    /**
     * @dataProvider invalidTimezoneProvider
     */
    public function test_that_timezone_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::timezone($value));
    }

    /**
     * @dataProvider validJsonProvider
     */
    public function test_that_json_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::json($value));
    }

    /**
     * @dataProvider invalidJsonProvider
     */
    public function test_that_json_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::json($value));
    }

    /**
     * @dataProvider validMatchProvider
     */
    public function test_that_match_returns_true_for_valid_value($value, $pattern)
    {
        $this->assertTrue(Test::match($value, $pattern));
    }

    /**
     * @dataProvider invalidMatchProvider
     */
    public function test_that_match_returns_false_for_invalid_value($value, $pattern)
    {
        $this->assertFalse(Test::match($value, $pattern));
    }

    /**
     * @dataProvider validContainsProvider
     */
    public function test_that_contains_returns_true_for_valid_value($value, $search)
    {
        $this->assertTrue(Test::contains($value, $search));
    }

    /**
     * @dataProvider invalidContainsProvider
     */
    public function test_that_contains_returns_false_for_invalid_value($value, $search)
    {
        $this->assertFalse(Test::contains($value, $search));
    }

    /**
     * @dataProvider validStartsWithProvider
     */
    public function test_that_starts_with_returns_true_for_valid_value($value, $search)
    {
        $this->assertTrue(Test::startsWith($value, $search));
    }

    /**
     * @dataProvider invalidStartsWithProvider
     */
    public function test_that_starts_with_returns_false_for_invalid_value($value, $search)
    {
        $this->assertFalse(Test::startsWith($value, $search));
    }

    /**
     * @dataProvider validEndsWithProvider
     */
    public function test_that_ends_with_returns_true_for_valid_value($value, $search)
    {
        $this->assertTrue(Test::endsWith($value, $search));
    }

    /**
     * @dataProvider invalidEndsWithProvider
     */
    public function test_that_ends_with_returns_false_for_invalid_value($value, $search)
    {
        $this->assertFalse(Test::endsWith($value, $search));
    }

    /**
     * @dataProvider validExactLengthProvider
     */
    public function test_that_exact_length_returns_true_for_valid_value($value, $length)
    {
        $this->assertTrue(Test::exactLength($value, $length));
    }

    /**
     * @dataProvider invalidExactLengthProvider
     */
    public function test_that_exact_length_returns_false_for_invalid_value($value, $length)
    {
        $this->assertFalse(Test::exactLength($value, $length));
    }

    /**
     * @dataProvider validMinLengthProvider
     */
    public function test_that_min_length_returns_true_for_valid_value($value, $minLength)
    {
        $this->assertTrue(Test::minLength($value, $minLength));
    }

    /**
     * @dataProvider invalidMinLengthProvider
     */
    public function test_that_min_length_returns_false_for_invalid_value($value, $minLength)
    {
        $this->assertFalse(Test::minLength($value, $minLength));
    }

    /**
     * @dataProvider validMaxLengthProvider
     */
    public function test_that_max_length_returns_true_for_valid_value($value, $maxLength)
    {
        $this->assertTrue(Test::maxLength($value, $maxLength));
    }

    /**
     * @dataProvider invalidMaxLengthProvider
     */
    public function test_that_max_length_returns_false_for_invalid_value($value, $maxLength)
    {
        $this->assertFalse(Test::maxLength($value, $maxLength));
    }

    /**
     * @dataProvider validRangeLengthProvider
     */
    public function test_that_range_length_returns_true_for_valid_value($value, $minLength, $maxLength)
    {
        $this->assertTrue(Test::rangeLength($value, $minLength, $maxLength));
    }

    /**
     * @dataProvider invalidRangeLengthProvider
     */
    public function test_that_range_length_returns_false_for_invalid_value($value, $minLength, $maxLength)
    {
        $this->assertFalse(Test::rangeLength($value, $minLength, $maxLength));
    }

    /**
     * @dataProvider validExactNumberProvider
     */
    public function test_that_exact_number_returns_true_for_valid_value($value, $number)
    {
        $this->assertTrue(Test::exactNumber($value, $number));
    }

    /**
     * @dataProvider invalidExactNumberProvider
     */
    public function test_that_exact_number_returns_false_for_invalid_value($value, $number)
    {
        $this->assertFalse(Test::exactNumber($value, $number));
    }

    /**
     * @dataProvider validMinNumberProvider
     */
    public function test_that_min_number_returns_true_for_valid_value($value, $minNumber)
    {
        $this->assertTrue(Test::minNumber($value, $minNumber));
    }

    /**
     * @dataProvider invalidMinNumberProvider
     */
    public function test_that_min_number_returns_false_for_invalid_value($value, $minNumber)
    {
        $this->assertFalse(Test::minNumber($value, $minNumber));
    }

    /**
     * @dataProvider validMaxNumberProvider
     */
    public function test_that_max_number_returns_true_for_valid_value($value, $maxNumber)
    {
        $this->assertTrue(Test::maxNumber($value, $maxNumber));
    }

    /**
     * @dataProvider invalidMaxNumberProvider
     */
    public function test_that_max_number_returns_false_for_invalid_value($value, $maxNumber)
    {
        $this->assertFalse(Test::maxNumber($value, $maxNumber));
    }

    /**
     * @dataProvider validRangeNumberProvider
     */
    public function test_that_range_number_returns_true_for_valid_value($value, $minNumber, $maxNumber)
    {
        $this->assertTrue(Test::rangeNumber($value, $minNumber, $maxNumber));
    }

    /**
     * @dataProvider invalidRangeNumberProvider
     */
    public function test_that_range_number_returns_false_for_invalid_value($value, $minNumber, $maxNumber)
    {
        $this->assertFalse(Test::rangeNumber($value, $minNumber, $maxNumber));
    }

    /**
     * @dataProvider validWholeNumberProvider
     */
    public function test_that_whole_number_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::wholeNumber($value));
    }

    /**
     * @dataProvider invalidWholeNumberProvider
     */
    public function test_that_whole_number_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::wholeNumber($value));
    }

    /**
     * @dataProvider validNaturalNumberProvider
     */
    public function test_that_natural_number_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::naturalNumber($value));
    }

    /**
     * @dataProvider invalidNaturalNumberProvider
     */
    public function test_that_natural_number_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::naturalNumber($value));
    }

    /**
     * @dataProvider validIntValueProvider
     */
    public function test_that_int_value_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::intValue($value));
    }

    /**
     * @dataProvider invalidIntValueProvider
     */
    public function test_that_int_value_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::intValue($value));
    }

    /**
     * @dataProvider validExactCountProvider
     */
    public function test_that_exact_count_returns_true_for_valid_value($value, $count)
    {
        $this->assertTrue(Test::exactCount($value, $count));
    }

    /**
     * @dataProvider invalidExactCountProvider
     */
    public function test_that_exact_count_returns_false_for_invalid_value($value, $count)
    {
        $this->assertFalse(Test::exactCount($value, $count));
    }

    /**
     * @dataProvider validMinCountProvider
     */
    public function test_that_min_count_returns_true_for_valid_value($value, $minCount)
    {
        $this->assertTrue(Test::minCount($value, $minCount));
    }

    /**
     * @dataProvider invalidMinCountProvider
     */
    public function test_that_min_count_returns_false_for_invalid_value($value, $minCount)
    {
        $this->assertFalse(Test::minCount($value, $minCount));
    }

    /**
     * @dataProvider validMaxCountProvider
     */
    public function test_that_max_count_returns_true_for_valid_value($value, $maxCount)
    {
        $this->assertTrue(Test::maxCount($value, $maxCount));
    }

    /**
     * @dataProvider invalidMaxCountProvider
     */
    public function test_that_max_count_returns_false_for_invalid_value($value, $maxCount)
    {
        $this->assertFalse(Test::maxCount($value, $maxCount));
    }

    /**
     * @dataProvider validRangeCountProvider
     */
    public function test_that_range_count_returns_true_for_valid_value($value, $minCount, $maxCount)
    {
        $this->assertTrue(Test::rangeCount($value, $minCount, $maxCount));
    }

    /**
     * @dataProvider invalidRangeCountProvider
     */
    public function test_that_range_count_returns_false_for_invalid_value($value, $minCount, $maxCount)
    {
        $this->assertFalse(Test::rangeCount($value, $minCount, $maxCount));
    }

    /**
     * @dataProvider validOneOfProvider
     */
    public function test_that_one_of_returns_true_for_valid_value($value, $choices)
    {
        $this->assertTrue(Test::oneOf($value, $choices));
    }

    /**
     * @dataProvider invalidOneOfProvider
     */
    public function test_that_one_of_returns_false_for_invalid_value($value, $choices)
    {
        $this->assertFalse(Test::oneOf($value, $choices));
    }

    /**
     * @dataProvider validKeyIssetProvider
     */
    public function test_that_key_isset_returns_true_for_valid_value($value, $key)
    {
        $this->assertTrue(Test::keyIsset($value, $key));
    }

    /**
     * @dataProvider invalidKeyIssetProvider
     */
    public function test_that_key_isset_returns_false_for_invalid_value($value, $key)
    {
        $this->assertFalse(Test::keyIsset($value, $key));
    }

    /**
     * @dataProvider validKeyNotEmptyProvider
     */
    public function test_that_key_not_empty_returns_true_for_valid_value($value, $key)
    {
        $this->assertTrue(Test::keyNotEmpty($value, $key));
    }

    /**
     * @dataProvider invalidKeyNotEmptyProvider
     */
    public function test_that_key_not_empty_returns_false_for_invalid_value($value, $key)
    {
        $this->assertFalse(Test::keyNotEmpty($value, $key));
    }

    /**
     * @dataProvider validEqualProvider
     */
    public function test_that_equal_returns_true_for_valid_value($value1, $value2)
    {
        $this->assertTrue(Test::equal($value1, $value2));
    }

    /**
     * @dataProvider invalidEqualProvider
     */
    public function test_that_equal_returns_false_for_invalid_value($value1, $value2)
    {
        $this->assertFalse(Test::equal($value1, $value2));
    }

    /**
     * @dataProvider validNotEqualProvider
     */
    public function test_that_not_equal_returns_true_for_valid_value($value1, $value2)
    {
        $this->assertTrue(Test::notEqual($value1, $value2));
    }

    /**
     * @dataProvider invalidNotEqualProvider
     */
    public function test_that_not_equal_returns_false_for_invalid_value($value1, $value2)
    {
        $this->assertFalse(Test::notEqual($value1, $value2));
    }

    /**
     * @dataProvider validSameProvider
     */
    public function test_that_same_returns_true_for_valid_value($value1, $value2)
    {
        $this->assertTrue(Test::same($value1, $value2));
    }

    /**
     * @dataProvider invalidSameProvider
     */
    public function test_that_same_returns_false_for_invalid_value($value1, $value2)
    {
        $this->assertFalse(Test::same($value1, $value2));
    }

    /**
     * @dataProvider validNotSameProvider
     */
    public function test_that_not_same_returns_true_for_valid_value($value1, $value2)
    {
        $this->assertTrue(Test::notSame($value1, $value2));
    }

    /**
     * @dataProvider invalidNotSameProvider
     */
    public function test_that_not_same_returns_false_for_invalid_value($value1, $value2)
    {
        $this->assertFalse(Test::notSame($value1, $value2));
    }

    /**
     * @dataProvider validSameTypeProvider
     */
    public function test_that_same_type_returns_true_for_valid_value($value1, $value2)
    {
        $this->assertTrue(Test::sameType($value1, $value2));
    }

    /**
     * @dataProvider invalidSameTypeProvider
     */
    public function test_that_same_type_returns_false_for_invalid_value($value1, $value2)
    {
        $this->assertFalse(Test::sameType($value1, $value2));
    }

    /**
     * @dataProvider validTypeProvider
     */
    public function test_that_type_returns_true_for_valid_value($value, $type)
    {
        $this->assertTrue(Test::type($value, $type));
    }

    /**
     * @dataProvider invalidTypeProvider
     */
    public function test_that_type_returns_false_for_invalid_value($value, $type)
    {
        $this->assertFalse(Test::type($value, $type));
    }

    /**
     * @dataProvider validListOfProvider
     */
    public function test_that_list_of_returns_true_for_valid_value($value, $type)
    {
        $this->assertTrue(Test::listOf($value, $type));
    }

    /**
     * @dataProvider invalidListOfProvider
     */
    public function test_that_list_of_returns_false_for_invalid_value($value, $type)
    {
        $this->assertFalse(Test::listOf($value, $type));
    }

    /**
     * @dataProvider validJsonEncodableProvider
     */
    public function test_that_json_encodable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::jsonEncodable($value));
    }

    /**
     * @dataProvider invalidJsonEncodableProvider
     */
    public function test_that_json_encodable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::jsonEncodable($value));
    }

    /**
     * @dataProvider validSerializableProvider
     */
    public function test_that_serializable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::serializable($value));
    }

    /**
     * @dataProvider invalidSerializableProvider
     */
    public function test_that_serializable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::serializable($value));
    }

    /**
     * @dataProvider validTraversableProvider
     */
    public function test_that_traversable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::traversable($value));
    }

    /**
     * @dataProvider invalidTraversableProvider
     */
    public function test_that_traversable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::traversable($value));
    }

    /**
     * @dataProvider validCountableProvider
     */
    public function test_that_countable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::countable($value));
    }

    /**
     * @dataProvider invalidCountableProvider
     */
    public function test_that_countable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::countable($value));
    }

    /**
     * @dataProvider validArrayAccessibleProvider
     */
    public function test_that_array_accessible_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::arrayAccessible($value));
    }

    /**
     * @dataProvider invalidArrayAccessibleProvider
     */
    public function test_that_array_accessible_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::arrayAccessible($value));
    }

    /**
     * @dataProvider validComparableProvider
     */
    public function test_that_comparable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::comparable($value));
    }

    /**
     * @dataProvider invalidComparableProvider
     */
    public function test_that_comparable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::comparable($value));
    }

    /**
     * @dataProvider validEquatableProvider
     */
    public function test_that_equatable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::equatable($value));
    }

    /**
     * @dataProvider invalidEquatableProvider
     */
    public function test_that_equatable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::equatable($value));
    }

    /**
     * @dataProvider validImplementsProvider
     */
    public function test_that_implements_returns_true_for_valid_value($value, $interface)
    {
        $this->assertTrue(Test::implements($value, $interface));
    }

    /**
     * @dataProvider invalidImplementsProvider
     */
    public function test_that_implements_returns_false_for_invalid_value($value, $interface)
    {
        $this->assertFalse(Test::implements($value, $interface));
    }

    /**
     * @dataProvider validInstanceOfProvider
     */
    public function test_that_instance_of_returns_true_for_valid_value($value, $className)
    {
        $this->assertTrue(Test::instanceOf($value, $className));
    }

    /**
     * @dataProvider invalidInstanceOfProvider
     */
    public function test_that_instance_of_returns_false_for_invalid_value($value, $className)
    {
        $this->assertFalse(Test::instanceOf($value, $className));
    }

    /**
     * @dataProvider validSubclassOfProvider
     */
    public function test_that_subclass_of_returns_true_for_valid_value($value, $className)
    {
        $this->assertTrue(Test::subclassOf($value, $className));
    }

    /**
     * @dataProvider invalidSubclassOfProvider
     */
    public function test_that_subclass_of_returns_false_for_invalid_value($value, $className)
    {
        $this->assertFalse(Test::subclassOf($value, $className));
    }

    /**
     * @dataProvider validClassExistsProvider
     */
    public function test_that_class_exists_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::classExists($value));
    }

    /**
     * @dataProvider invalidClassExistsProvider
     */
    public function test_that_class_exists_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::classExists($value));
    }

    /**
     * @dataProvider validMethodExistsProvider
     */
    public function test_that_method_exists_returns_true_for_valid_value($value, $object)
    {
        $this->assertTrue(Test::methodExists($value, $object));
    }

    /**
     * @dataProvider invalidMethodExistsProvider
     */
    public function test_that_method_exists_returns_false_for_invalid_value($value, $object)
    {
        $this->assertFalse(Test::methodExists($value, $object));
    }

    /**
     * @dataProvider validPathProvider
     */
    public function test_that_path_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::path($value));
    }

    /**
     * @dataProvider invalidPathProvider
     */
    public function test_that_path_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::path($value));
    }

    /**
     * @dataProvider validFileProvider
     */
    public function test_that_file_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::file($value));
    }

    /**
     * @dataProvider invalidFileProvider
     */
    public function test_that_file_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::file($value));
    }

    /**
     * @dataProvider validDirProvider
     */
    public function test_that_dir_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::dir($value));
    }

    /**
     * @dataProvider invalidDirProvider
     */
    public function test_that_dir_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::dir($value));
    }

    /**
     * @dataProvider validReadableProvider
     */
    public function test_that_readable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::readable($value));
    }

    /**
     * @dataProvider invalidReadableProvider
     */
    public function test_that_readable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::readable($value));
    }

    /**
     * @dataProvider validWritableProvider
     */
    public function test_that_writable_returns_true_for_valid_value($value)
    {
        $this->assertTrue(Test::writable($value));
    }

    /**
     * @dataProvider invalidWritableProvider
     */
    public function test_that_writable_returns_false_for_invalid_value($value)
    {
        $this->assertFalse(Test::writable($value));
    }
}
