<?php

namespace Novuso\Test\System\Doubles;

use Novuso\System\Type\Enum;

final class WeekDay extends Enum
{
    const SUNDAY = 0;
    const MONDAY = 1;
    const TUESDAY = 2;
    const WEDNESDAY = 3;
    const THURSDAY = 4;
    const FRIDAY = 5;
    const SATURDAY = 6;
}
